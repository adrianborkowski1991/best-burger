# Best Burger

## About project

Best Burger is a web application that allows users to order burgers online.
The application is divided into a front-end and a backend, which operate in a distributed mode, communicating with each other through a RESTful API mechanism. Both layers of the application are organized into separate folders. 
The application includes the following features:
- Displaying products based on defined categories,
- Adding products to the shopping cart,
- Returning to the page with payment confirmation request.


## 💡 Technologies

Both the back-end and front-end were built using **JavaScript**, reinforced by the static typing tool **TypeScript**.

The front-end was crafted using the **Vite** tool in conjunction with the **React.js** framework. Examples of additional libraries:
- **Redux Toolkit**
- **Styled component**
- **Axios**

Back-end was developed using **Node.js** with support from the **Express** framework. The database used is **MySQL**. Examples of additional libraries:
- **Sequelize**
- **Winston**
- **Express-session**

## 💿 Installation

To install and run the back-end environment, you should use the following commands:

```
cd backend
npm install
npm start
```

For front-end is it:

```
cd frontend
npm install
npm run dev
```
## 📡 Live version
Check out the "Best burger" project in [DEMO mode](https://best-burger-adrianborkowski.pl/).

## 🧪 Testing
Example tests for the 'CartModal' component have been prepared in the frontend part, and they are built using **Jest** and **React Testing Library**.
Path: /frontend/src/components/layout/cart-modal/cart-modal.test.tsx [Show](https://gitlab.com/adrianborkowski1991/best-burger/-/blob/main/frontend/src/components/layout/cart-modal/cart-modal.test.tsx?ref_type=heads)

To run the tests, enter the following commands:

```
cd frontend
npm test
```
