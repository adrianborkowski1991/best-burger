declare module 'express-session' {
    interface SessionData {
        cart: {
            id: number
        } | null;
    }
}

export {};