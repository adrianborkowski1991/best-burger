import bodyParser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import * as dotenv from 'dotenv';
import express from 'express';
import helmet from 'helmet';

import corsConfig from './config/cors-config';
import database from './config/database-config';
import errorResponse from './middlewares/error-response';
import sessionExpress from './middlewares/session-express';
import staticDirectory from './middlewares/static-directory';
import configAssociation from './models/associations';
import categoryRoutes from './routes/category-routes';
import cartRoutes from './routes/cart-routes';
import orderRoutes from './routes/order-routes';
import productRoutes from './routes/product-routes';
import ErrorResponse from './utils/error-response/error-response';
import {ErrorType} from './utils/error-response/error-response.types';
import bulkData from './utils/data/data-loader';
import {EnvironmentalVariable, generateEnvironmentErrorLog} from './utils/environment-error';
import ResponseResult from './utils/response-result/response-result';

dotenv.config();

const app = express();
app.set('trust proxy', 1);

app.use(cors(corsConfig));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(...staticDirectory());

app.use(sessionExpress())

app.use('/api/product', productRoutes);
app.use('/api/category', categoryRoutes);
app.use('/api/cart', cartRoutes);
app.use('/api/order', orderRoutes);

app.use(helmet());
app.use(compression());
app.use(errorResponse);

configAssociation();

if(!process.env.SERVER_PORT) {
    throw new ErrorResponse(ResponseResult.serverUnavailable(generateEnvironmentErrorLog(EnvironmentalVariable.SERVER_PORT)), ErrorType.SERVER);
}

database
    .sync({ force: true })
    .then(async () => {
        Boolean(process.env.DATABASE_DEPLOY_SCRIPT) && await bulkData();
        app.listen(process.env.SERVER_PORT);
    })
    .catch(() => new ErrorResponse(ResponseResult.serverUnavailable(), ErrorType.SERVER));
