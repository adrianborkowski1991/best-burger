import express from 'express';

import {getCart, putCartAddProduct, putCartRemoveProduct} from '../controllers/cart-controller';

const router = express.Router();

router.get('/', getCart);
router.put('/add-product', putCartAddProduct);
router.put('/remove-product', putCartRemoveProduct);

export default router;