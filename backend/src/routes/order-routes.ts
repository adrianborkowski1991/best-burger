import express from 'express';

import {postOrder, putOrderPaid} from '../controllers/order-controller';

const router = express.Router();

router.post('/', postOrder);
router.put('/paid/:id', putOrderPaid);
export default router;