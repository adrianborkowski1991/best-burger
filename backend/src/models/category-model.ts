import Sequelize, {Model, Optional} from 'sequelize';

import {IdAttribute, ModelStatic} from './common';
import database from '../config/database-config';

export interface CategoryAttributes extends IdAttribute{
    name: string;
    iconUrl: string;
}
type CategoryCreationAttributes = Optional<CategoryAttributes, 'id'>

export interface CategoryInstance
    extends Model<CategoryAttributes, CategoryCreationAttributes>,
        CategoryAttributes {}

export const CategoryModel = <ModelStatic<CategoryInstance>>database.define<CategoryInstance>('category', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    iconUrl: {
        type: Sequelize.STRING,
        allowNull: false
    }
}, {
    timestamps: false
});