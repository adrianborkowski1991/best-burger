import {CartModel} from './cart-model';
import {CategoryModel} from './category-model';
import {OrderProductsModel} from './order-products-model';
import {ProductModel} from './product-model';

const configAssociation = () => {
    ProductModel.belongsTo(CategoryModel, { as: 'category' });

    CartModel.belongsToMany(ProductModel, { through: OrderProductsModel });
    ProductModel.belongsToMany(CartModel, { through: OrderProductsModel });
}

export default configAssociation;