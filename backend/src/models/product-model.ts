import Sequelize, { Model, Optional} from 'sequelize';

import {IdAttribute, ModelStatic} from './common';
import database from '../config/database-config';

export interface ProductCommon {
    title: string;
    price: number;
    imageUrl: string;
}
export interface ProductAttributes extends IdAttribute, ProductCommon{
    new: boolean;
    categoryId?: number;
}

type ProductCreationAttributes = Optional<ProductAttributes, 'id'>

export interface ProductInstance
    extends Model<ProductAttributes, ProductCreationAttributes>,
        ProductAttributes {}

export const ProductModel = <ModelStatic<ProductInstance>>database.define('product', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    price: {
        type: Sequelize.DOUBLE,
        allowNull: false
    },
    imageUrl: {
        type: Sequelize.STRING,
        allowNull: false
    },
    new: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
},{
    timestamps: false
});
