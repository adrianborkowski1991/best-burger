import Sequelize, {Model, Optional} from 'sequelize';

import {IdAttribute, ModelStatic} from './common';
import database from '../config/database-config';

export interface CartAttributes extends IdAttribute{
    active: boolean;
}
type CartCreationAttributes = Optional<CartAttributes, 'id'>

export interface CartInstance
    extends Model<CartAttributes, CartCreationAttributes>,
        CartAttributes {}
export const CartModel =  <ModelStatic<CartInstance>>database.define('cart', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    active: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
});
