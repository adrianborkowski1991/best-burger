import Sequelize, {Model, Optional} from 'sequelize';

import {IdAttribute, ModelStatic} from './common';
import database from '../config/database-config';

export type OrderStatus = 'NEW' | 'PAID' | 'SENT' | 'CLOSED' | 'CANCELED';
const orderStatusArray: OrderStatus[] = ['NEW',  'PAID', 'SENT', 'CLOSED', 'CANCELED'];

export interface OrderAttributes extends IdAttribute{
    address: string;
    fullName: string;
    email: string;
    phone: string;
    status?: OrderStatus;
}
type OrderCreationAttributes = Optional<OrderAttributes, 'id'>

export interface OrderInstance
    extends Model<OrderAttributes, OrderCreationAttributes>,
        OrderAttributes {}
export const OrderModel= <ModelStatic<OrderInstance>>database.define<OrderInstance>('order', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    address: {
        type: Sequelize.STRING,
        allowNull: false
    },
    fullName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    phone: {
        type: Sequelize.STRING,
        allowNull: false
    },
    status :  {
        type: Sequelize.ENUM<OrderStatus>(...orderStatusArray),
        allowNull: false,
        defaultValue: 'NEW'
    },
}, {
    timestamps: true
});