import {BuildOptions, Model} from 'sequelize';

export interface IdAttribute {
    id?: number;
}

export type ModelStatic<T> = typeof Model
    & { associate: (models: any) => void }
    & { new(values?: Record<string, unknown>, options?: BuildOptions): T }

