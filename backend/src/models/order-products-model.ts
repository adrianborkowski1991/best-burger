import Sequelize, {Model, Optional} from 'sequelize';

import {IdAttribute, ModelStatic} from './common';
import database from '../config/database-config';

export interface OrderProductsAttributes extends IdAttribute{
    quantity: number;
    productId: number;
    orderId?: number | null;
    cartId?: number;
}
type OrderProductsCreationAttributes = Optional<OrderProductsAttributes, 'id'>

export interface OrderProductsInstance
    extends Model<OrderProductsAttributes, OrderProductsCreationAttributes>,
        OrderProductsAttributes {}
export const OrderProductsModel =  <ModelStatic<OrderProductsInstance>>database.define('orderProducts', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    orderId: {
        type: Sequelize.INTEGER
    },
    quantity: Sequelize.INTEGER
}, {
    timestamps: false
});
