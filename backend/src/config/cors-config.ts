const corsConfig =  {
    origin: ['https://best-burger-adrianborkowski.pl', 'http://localhost:5173'],
    credentials: true,
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
    exposedHeaders: ['set-cookie']
};

export default corsConfig;