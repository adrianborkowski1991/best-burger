import { Sequelize } from 'sequelize';
import * as dotenv from 'dotenv';

import {EnvironmentalVariable, generateEnvironmentErrorLog} from '../utils/environment-error';
import ErrorResponse from '../utils/error-response/error-response';
import {ErrorType} from '../utils/error-response/error-response.types';
import ResponseResult from '../utils/response-result/response-result';

dotenv.config();

const databaseConfig = () => {
    if(!process.env.DATABASE_NAME || !process.env.DATABASE_USER || !process.env.DATABASE_PASSWORD || !process.env.DATABASE_HOST){
        throw new ErrorResponse(ResponseResult.serverUnavailable(
            generateEnvironmentErrorLog([EnvironmentalVariable.DATABASE_NAME, EnvironmentalVariable.DATABASE_USER, EnvironmentalVariable.DATABASE_PASSWORD, EnvironmentalVariable.DATABASE_HOST])), ErrorType.SERVER);
    }

    return new Sequelize(
        process.env.DATABASE_NAME,
        process.env.DATABASE_USER,
        process.env.DATABASE_PASSWORD,{
            dialect: 'mysql',
            host: process.env.DATABASE_HOST
    });
}

const database = databaseConfig();

export default database;