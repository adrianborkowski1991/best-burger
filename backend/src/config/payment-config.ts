type PaymentMethodType =  'blik' | 'card' | 'p24' | 'paynow' | 'paypal';

export interface StripeConfig {
    payment: 'payment' | 'setup' | 'subscription'
    apiVersion: '2023-08-16',
    paymentMethodType: PaymentMethodType[];
    currency: 'eur' | 'pln' | 'usd'
}
