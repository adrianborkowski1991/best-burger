import session from 'express-session';

import {EnvironmentalVariable, generateEnvironmentErrorLog} from '../utils/environment-error';
import ErrorResponse from '../utils/error-response/error-response';
import {ErrorType} from '../utils/error-response/error-response.types';
import ResponseResult from '../utils/response-result/response-result';

const sessionExpress = () => {
    if(!process.env.SESSION_SECRET){
        throw new ErrorResponse(ResponseResult.serverUnavailable(generateEnvironmentErrorLog(EnvironmentalVariable.SESSION_SECRET)), ErrorType.SERVER);
    }

    return session({
        name: "session.best-burger",
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: true,
        proxy: true,
        cookie: {
            maxAge: 7 * 24 * 3600000,
            secure: true,
            httpOnly: true,
            sameSite: 'none'
        }
    })
}

export default sessionExpress;