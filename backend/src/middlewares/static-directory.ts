import express from 'express';
import {fileURLToPath} from 'node:url';
import path from 'node:path';

const staticDirectory = () : [string, express.RequestHandler] => {
    const __dirname = path.dirname(fileURLToPath(import.meta.url));
    return ['/public', express.static(path.join(__dirname, '..', 'public'))]
}

export default staticDirectory