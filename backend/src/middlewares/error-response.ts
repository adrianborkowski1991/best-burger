import {NextFunction, Request, Response} from 'express';

import ErrorResponse from '../utils/error-response/error-response';
import {ErrorType} from '../utils/error-response/error-response.types';
import ResponseResult from '../utils/response-result/response-result';
import logger from '../utils/logger';

const errorResponse = (err: ErrorResponse, _: Request, res: Response, _2: NextFunction) => {
  const result = err.responseResult || ResponseResult.serverUnavailable();
  const {status, message, operationStatus} = result;

  logger.log({
    level: 'error',
    message: `${new Date().toISOString()}. ${err.type ?? ErrorType.SERVER}. ${result.log ?? err.toString()} Response status code : ${
        status
    }. `
  });

  res.status(status).json({
    status,
    message,
    operationStatus
  });
};

export default errorResponse;