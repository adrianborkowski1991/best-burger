import * as dotenv from 'dotenv';
import Stripe from 'stripe';

import {StripeConfig} from '../config/payment-config';
import {ModelStatic} from '../models/common';
import {OrderProductsAttributes} from '../models/order-products-model';
import {ProductInstance} from '../models/product-model';
import {EnvironmentalVariable, generateEnvironmentErrorLog} from '../utils/environment-error';
import ErrorResponse from '../utils/error-response/error-response';
import {ErrorType} from '../utils/error-response/error-response.types';
import ResponseResult from '../utils/response-result/response-result';

dotenv.config();

export default class PaymentService{
    private readonly _stripe: Stripe;
    private static readonly STRIPE_CONFIG : StripeConfig = {
        payment: 'payment',
        apiVersion: '2023-08-16',
        paymentMethodType: ['card'],
        currency: 'pln'
    }

    constructor(private _productModel: ModelStatic<ProductInstance>, private _config = PaymentService.STRIPE_CONFIG) {
        this._stripe = this.createStripeObject();
    }

    async generatePaymentLink(orderProducts: OrderProductsAttributes[], email: string, orderId: number) {
        const stripe = await this.createStripeObject();

        if(!process.env.STRIPE_RETURN_URL){
            throw new ErrorResponse(ResponseResult.serverUnavailable(generateEnvironmentErrorLog(EnvironmentalVariable.STRIPE_RETURN_URL)), ErrorType.PAYMENT);
        }

        const lineItems = await this.createLineItems(orderProducts);

        const session = await stripe.checkout.sessions.create({
            customer_email: email,
            payment_method_types: this._config.paymentMethodType,
            line_items: lineItems,
            mode: this._config.payment,
            success_url: process.env.STRIPE_RETURN_URL+`?orderId=${orderId}`,
            cancel_url: process.env.STRIPE_RETURN_URL,
        });

        if(!session.url){
            throw new ErrorResponse(ResponseResult.serverUnavailable('Payment link was not generated'), ErrorType.ORDER);
        }

        return session.url;
    }

    private createStripeObject(){
        if(!process.env.STRIPE_SECRET_KEY){
            throw new ErrorResponse(ResponseResult.serverUnavailable(generateEnvironmentErrorLog(EnvironmentalVariable.STRIPE_SECRET_KEY)), ErrorType.PAYMENT);
        }

        return new Stripe(process.env.STRIPE_SECRET_KEY, {
            apiVersion: this._config.apiVersion,
        });
    }

    private async createLineItems(orderProducts: OrderProductsAttributes[]) {
        const productIds = orderProducts.map(product => product.productId);

        const products = await this._productModel.findAll({
            where: {
                id: productIds
            }
        });

        return products.map(product => {
            const orderProduct = orderProducts.find(orderProduct => orderProduct.productId === product.id)

            return {
                price_data: {
                    currency: this._config.currency,
                    product_data: {
                        name: product.title,
                    },
                    unit_amount: product.price * 100,
                },
                quantity: orderProduct?.quantity,
            }
        });
    }

}

