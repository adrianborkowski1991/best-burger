import {ModelStatic} from '../models/common';
import {ErrorType} from '../utils/error-response/error-response.types';

export default abstract class GeneralService<T> {
    protected constructor(protected _model: ModelStatic<T>, protected _errorType: ErrorType) {}
}