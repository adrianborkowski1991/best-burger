
import GeneralService from './general-service';
import {CartPack, CartProducts} from './services-types';

import {CartInstance} from '../models/cart-model';
import {ModelStatic} from '../models/common';
import {OrderProductsInstance} from '../models/order-products-model';
import {ProductInstance} from '../models/product-model';
import ErrorResponse from '../utils/error-response/error-response';
import {ErrorType} from '../utils/error-response/error-response.types';
import ResponseResult from '../utils/response-result/response-result';

export default class CartService extends GeneralService<OrderProductsInstance>{

    constructor(_orderProductsModel: ModelStatic<OrderProductsInstance>, private _cartModel: ModelStatic<CartInstance>, private _productModel: ModelStatic<ProductInstance>) {
        super(_orderProductsModel, ErrorType.CART);
    }

    async readCartProducts(cartId: number) : Promise<CartPack> {
        const cart = await this.getCart(cartId);
        const orderProducts = await this._model.findAll({where: {cartId}});
        let cartProducts : CartProducts[] = [];

        for (const orderProduct of orderProducts) {
            const findProduct = await this._productModel.findByPk(orderProduct.productId);

            if(findProduct){
                cartProducts.push({
                    ...orderProduct.dataValues,
                    ...findProduct.dataValues
                });
            }
        }

        cartProducts = this.sortCartProducts(cartProducts);

        return {
            cart, cartProducts : cartProducts ?? []
        };
    }

    async createCart() : Promise<CartPack> {
        const cart =  await this._cartModel.create( { active: true });

        return {
            cart, cartProducts: []
        };
    }

    async addProductToCart(cartId: number, cartProductToAdd: OrderProductsInstance, productsCart: CartProducts[]) : Promise<CartProducts[]>{
        const findOrderProduct = await this._model.findOne({where : {cartId, productId: cartProductToAdd.productId}});
        const findProduct = await this._productModel.findByPk(cartProductToAdd.productId);

        let newProductsCart = [...productsCart];

        if(findOrderProduct && findProduct){
            findOrderProduct.quantity = cartProductToAdd.quantity;

            const [updatedCount] = await this._model.update(findOrderProduct.dataValues, {
                where: {
                    productId: cartProductToAdd.productId,
                    cartId
                }
            });

            if (updatedCount) {
                newProductsCart = newProductsCart.filter(product => product.productId !== cartProductToAdd.productId);
                newProductsCart.push({...findOrderProduct.dataValues, ...findProduct.dataValues});
            }
        }
        else if(findProduct){
            const createQuery = await this._model.create(cartProductToAdd);

            if(createQuery){
                createQuery.orderId = null;
                newProductsCart.push({...createQuery.dataValues, ...findProduct.dataValues});
            }
        }

        newProductsCart = this.sortCartProducts(newProductsCart);

        return newProductsCart;
    }

    async deleteProductFromCart(cartId: number, idCartProductToDelete: number, productsCart: CartProducts[]) : Promise<CartProducts[]>{
        let productsCartCopy = [...productsCart];
        const deleteQuery = await this._model.findOne({where : {cartId, productId: idCartProductToDelete}});

        if(deleteQuery){
            await deleteQuery.destroy();
        }

        productsCartCopy = this.sortCartProducts(productsCartCopy);

        return deleteQuery ? productsCart.filter(product => product.productId !== idCartProductToDelete) : productsCartCopy;
    }

    async deactivateCart(cartId: number){
        const [updatedCount] = await this._cartModel.update({active: false}, {
            where: {
                id: cartId
            }
        });

        if (!updatedCount) {
            const errorMessage = `The operation was not processed. No cart with ID ${cartId} found or it was already deactivated.`;
            throw new ErrorResponse(ResponseResult.unprocessableEntity(errorMessage), this._errorType);
        }
    }

    private async getCart(cartId: number) {
        const cartFind = await this._cartModel.findByPk(cartId);

        if(!cartFind){
            throw new ErrorResponse(ResponseResult.notFound(), this._errorType);
        }

        return cartFind;
    }

    private sortCartProducts(cartProducts: CartProducts[]) {
        return cartProducts.sort((a, b) => a.title.localeCompare(b.title));
    }
}