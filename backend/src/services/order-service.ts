import GeneralService from './general-service';

import {CartPack} from './services-types';
import {ModelStatic} from '../models/common';
import {OrderAttributes, OrderInstance} from '../models/order-model';
import {OrderProductsInstance} from '../models/order-products-model';
import ErrorResponse from '../utils/error-response/error-response';
import {ErrorType} from '../utils/error-response/error-response.types';
import ResponseResult from '../utils/response-result/response-result';

export default class OrderService extends GeneralService<OrderProductsInstance>{

    constructor(_orderProductsModel: ModelStatic<OrderProductsInstance>, private _orderModel: ModelStatic<OrderInstance>) {
        super(_orderProductsModel, ErrorType.ORDER);
    }

    async paidOrder(orderId: number) : Promise<OrderInstance>{
        const findOrder = await this._orderModel.findByPk(orderId);

        if(!findOrder){
            throw new ErrorResponse(ResponseResult.notFound(), this._errorType);
        }

        findOrder.dataValues.status = 'PAID';
        const [updatedCount] = await this._orderModel.update(
            {
                status: 'PAID'
            }, {
            where: {
                id: orderId
            }
        });

        if (updatedCount === 0) {
            const errorMessage = `The operation was not processed.`;
            throw new ErrorResponse(ResponseResult.unprocessableEntity(errorMessage), this._errorType);
        }

        return findOrder;
    }

    async createOrder(cartPack: CartPack, order: OrderAttributes){
        const cartId = cartPack.cart.id;

        if(!cartId){
            const errorMessage = `The operation was not processed. No cart with ID ${cartId} found to create order.`;
            throw new ErrorResponse(ResponseResult.unprocessableEntity(errorMessage), this._errorType);
        }

        const orderToCreate = {...order};
        orderToCreate.status = 'NEW';
        const orderCreated = await this._orderModel.create(orderToCreate);

        const orderProductsToUpdate = await this._model.findAll({
            where: { cartId }
        });

        if(orderProductsToUpdate.length === 0){
            throw new ErrorResponse(ResponseResult.unprocessableEntity('Some issue! The order doesn\'t have any items.'), this._errorType);
        }
        else {
            const orderProductIdsToUpdate = orderProductsToUpdate.map((product) => Number(product.dataValues.id));

            let updatedCountTotal = 0;
            for (const orderProductId of orderProductIdsToUpdate) {
                const [updatedCount] = await this._model.update(
                    { orderId: orderCreated.id },
                    {
                        where: {
                            id: orderProductId,
                        }
                    }
                );
                updatedCountTotal += updatedCount;
            }

            if (updatedCountTotal === 0) {
                const errorMessage = `The operation was not processed. Order does not contain any products.`;
                throw new ErrorResponse(ResponseResult.unprocessableEntity(errorMessage), this._errorType);
            }

            return orderCreated.id;
        }
    }
}