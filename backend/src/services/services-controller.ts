
import CartService from './cart-service';
import CategoryService from './category-service';
import EmailService from './email-service';
import OrderService from './order-service';
import PaymentService from './payment-service';
import ProductService from './product-service';
import {CartModel} from '../models/cart-model';
import {CategoryModel} from '../models/category-model';
import {OrderModel} from '../models/order-model';
import {OrderProductsModel} from '../models/order-products-model';
import {ProductModel} from '../models/product-model';

const emailService = new EmailService();
const categoryService = new CategoryService(CategoryModel);
const productService = new ProductService(ProductModel);
const cartService = new CartService(OrderProductsModel, CartModel, ProductModel);
const paymentService = new PaymentService(ProductModel);
const orderService = new OrderService(OrderProductsModel, OrderModel);

export const serviceController = {
    emailService,
    categoryService,
    productService,
    cartService,
    paymentService,
    orderService
}