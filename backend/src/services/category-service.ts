import GeneralService from './general-service';
import {CategoryInstance } from '../models/category-model';
import {ModelStatic} from '../models/common';
import {ErrorType} from '../utils/error-response/error-response.types';
import ErrorResponse from '../utils/error-response/error-response';
import ResponseResult from '../utils/response-result/response-result';

export default class CategoryService extends GeneralService<CategoryInstance>{
    constructor(_categoryModel: ModelStatic<CategoryInstance>) {
        super(_categoryModel, ErrorType.CATEGORY);
    }

    async readCategoriesAll() : Promise<CategoryInstance[]> {
        return await this._model.findAll();
    }

    async readCategoryByName(name: string) : Promise<CategoryInstance> {
        const find = await this._model.findOne({
            where: { name }
        });

        if(!find){
            throw new ErrorResponse(ResponseResult.notFound(), this._errorType);
        }

        return find;
    }

}