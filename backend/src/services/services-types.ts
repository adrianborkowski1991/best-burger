import {CartInstance} from '../models/cart-model';
import {ProductCommon} from '../models/product-model';

export interface CartPack{
    cartProducts: CartProducts[];
    cart: CartInstance;
}

export interface CartProducts extends ProductCommon {
    quantity: number;
    productId: number;
}