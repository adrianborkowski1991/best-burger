import Sequelize from 'sequelize';

import GeneralService from './general-service';
import {ModelStatic} from '../models/common';
import {ProductInstance } from '../models/product-model';
import {ErrorType} from '../utils/error-response/error-response.types';

export default class ProductService extends GeneralService<ProductInstance> {
    constructor(_productModel: ModelStatic<ProductInstance>) {
        super(_productModel, ErrorType.PRODUCT);
    }

    async readProductsByIds(ids: number[]) : Promise<ProductInstance[]>{
        return this.sortCartProducts(await this._model.findAll({
            where: {
                id: {
                    [Sequelize.Op.in]: ids,
                },
            }
        }));
    }

    async readProductsByCategoryId(categoryId: number) : Promise<ProductInstance[]>{
        return this.sortCartProducts(await this._model.findAll({
            where: {categoryId}
        }));
    }

    async readProductsByNewAttribute(isNew: boolean) : Promise<ProductInstance[]>{
        return this.sortCartProducts(await this._model.findAll({
            where: { new: isNew }
        }));
    }

    private sortCartProducts(products: ProductInstance[]) {
        return products.sort((a, b) => a.title.localeCompare(b.title));
    }
}

