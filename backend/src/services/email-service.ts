import dotenv from 'dotenv';
import nodemailer from 'nodemailer';

import ErrorResponse from '../utils/error-response/error-response';
import {ErrorType} from '../utils/error-response/error-response.types';
import ResponseResult from '../utils/response-result/response-result';

dotenv.config();

export default class EmailService {

  private transporter: nodemailer.Transporter;
  private readonly emailFrom: string;

  constructor() {
    if(process.env.EMAIL_HOST && process.env.EMAIL_USERNAME && process.env.EMAIL_PASSWORD && process.env.EMAIL_PORT){
      this.transporter = nodemailer.createTransport({
        host: process.env.EMAIL_HOST,
        port: Number(process.env.EMAIL_PORT),
        secure: true,
        auth: {
          user: process.env.EMAIL_USERNAME,
          pass: process.env.EMAIL_PASSWORD,
        },
      });
      this.emailFrom = process.env.EMAIL_USERNAME;
    }
  }

  async sendEmail(to: string, subject: string, content: string) {
    try {
      return await this.transporter.sendMail({
        from: this.emailFrom,
        to,
        subject,
        html: content,
      });
    } catch (error) {
      new ErrorResponse(ResponseResult.serverUnavailable('An error occurred while sending the email. Please try again later.'),  ErrorType.EMAIL)
    }
  }
}