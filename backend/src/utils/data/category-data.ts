export const categoryData  = [
    {
        id: 1,
        name: 'news',
        iconUrl: '/public/category-news.png',
    },
    {
        id: 2,
        name: 'burgers',
        iconUrl: '/public/category-burger.png',
    },
    {
        id: 3,
        name: 'salads',
        iconUrl: '/public/category-salad.png',
    },
    {
        id: 4,
        name: 'sides',
        iconUrl: '/public/category-side.png',
    },
    {
        id: 5,
        name: 'drinks',
        iconUrl: '/public/category-drink.png',
    }
]