import {productData} from './product-data';
import {categoryData} from './category-data';
import ResponseResult from '../response-result/response-result';
import database from '../../config/database-config';
import ErrorResponse from '../error-response/error-response';
import {ErrorType} from '../error-response/error-response.types';
import {CategoryModel} from '../../models/category-model';
import {ProductModel} from '../../models/product-model';


 const bulkData  = async () => {
    try {
        await database.sync();
        await CategoryModel.bulkCreate(categoryData);
        await ProductModel.bulkCreate(productData);
    } catch (error) {
        throw new ErrorResponse(ResponseResult.serverUnavailable(`An error occurred while bulk data import: ${error}`), ErrorType.SERVER);
    }
}

export default bulkData;