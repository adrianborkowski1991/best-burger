import {isArray} from 'class-validator';

export enum EnvironmentalVariable {
    STRIPE_RETURN_URL = 'STRIPE_RETURN_URL',
    STRIPE_SECRET_KEY = 'STRIPE_SECRET_KEY',
    SERVER_PORT = 'SERVER_PORT',
    SESSION_SECRET = 'SESSION_SECRET',
    DATABASE_NAME = 'DATABASE_NAME',
    DATABASE_USER = 'DATABASE_USER',
    DATABASE_PASSWORD = 'DATABASE_PASSWORD',
    DATABASE_HOST = 'DATABASE_HOST'
}

export const generateEnvironmentErrorLog = (env: EnvironmentalVariable | EnvironmentalVariable[]) => {
    return `The environment variables: '${isArray(env) ? env.join(', ') : env}' has not been added`
}