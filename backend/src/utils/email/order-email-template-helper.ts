import {ProductInstance} from '../../models/product-model';
import {CartProducts} from '../../services/services-types';

export type ColumnConfig = {content: string, widthPercentage: number};
export class OrderEmailTemplateHelper {

    static async orderConfirmation(items: CartProducts[], products: ProductInstance[], idOrder: number){
        let total = 0;
        const productsToEmail = items.map(item=> {
            const productElement = products.find(element=> element.dataValues.id === item.productId);

            total += productElement?.price ? productElement?.price * item.quantity : 0;

            if(productElement){
                return {
                    ...productElement.dataValues,
                    ...item,
                }
            }

        })

        const orderElements = productsToEmail.map((productToEmail, index) => {
            if(!productToEmail){
                return null;
            }
            return {
                firstColumn: `${index +1}.`,
                secondColumn: `${productToEmail.title} (${productToEmail.quantity} PC) <em>(${productToEmail.price.toFixed(2)} PLN)</em>`,
                thirdColumn: `<strong>${(productToEmail.quantity * productToEmail.price).toFixed(2)} PLN</strong>`
            }
        });

        orderElements.push({
                firstColumn: '',
                secondColumn:  'Delivery',
                thirdColumn: '<strong>15.00 PLN</strong>',
            },
            {
                firstColumn: '',
                secondColumn:  'Total',
                thirdColumn: `<strong>${(total + 15).toFixed(2)} PLN</strong>`,
            }
        );

        const itemsElement = orderElements.reduce((combinedElement, element) => {
            const elementString  = element ? this._row(
                {
                    content: `<strong>${element.firstColumn}</strong>`,
                    widthPercentage: 10
                },{
                    content: element.secondColumn,
                    widthPercentage: 70
                },{
                    content: element.thirdColumn,
                    widthPercentage: 20
                }) : '';
            return combinedElement + (elementString ? elementString + '\n' : '');
        }, '');


        return this._wrap(idOrder, itemsElement);
    }

    static async orderPaid(idOrder: number){
        return this._wrap(idOrder, this._row({
            content: `<p><strong>Thank you for payment of your order. The delivery will be made within an hour.</strong></p>`,
            widthPercentage: 100,
        }));
    }

    private static _wrap(idOrder: number, main: string) {
        const title = `Order #${String(idOrder).padStart(5, '0')}`;

        return (
            `<!DOCTYPE html>
            <html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
            <head>
            <title></title>
            <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
            <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
            <style>
            * {
               box-sizing: border-box;
            }
            
            body {
                margin: 0;
                padding: 0;
            }

            p {
                line-height: inherit
            }
            
            tr, td {
                padding: 0;
                margin: 0;
            }

            @media (max-width:690px) {
                .row-content {
                    width: 100%;
                }
            
                .column {
                    width: 100%;
                    display: block;
                }

            }
            </style>
            </head>
                         
            <body style="background-color: #f9f9f9;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #f9f9f9;" >
                    <tbody>
                        <tr>
                            <td>
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" style="color: #000;" width="670">
                                                    <tbody>
                                                        <tr>
                                                            <td class="column" width="100%" style="vertical-align: top;">
                                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <h1 style="color: #1e0e4b; font-size: 38px; font-weight: 700; letter-spacing: normal; line-height: 120%; text-align: left;">
                                                                                <span>${title}</span>
                                                                            </h1>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                ${main}          
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
            </html>
        `)
    }

    private static _row(...columns: ColumnConfig[]) {
        const content = columns.reduce((combinedColumns, column) => {
            const elementString  =
                `<td class="column" width="${column.widthPercentage}%" style="vertical-align: top;" >
                 <table border="0" cellpadding="10" cellspacing="0"  width="100%" style="word-break: break-word;">
                     <tr>
                          <td style="color:#444a5b; font-size:16px; font-weight:400; line-height:120%; text-align:left;">
                               <p>
                                  ${column.content}
                               </p>
                          </td>
                     </tr>
                 </table>
            </td>`
            return combinedColumns + (elementString ? elementString + '\n' : '');
        }, '');

        return(
            `<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" width="670">
                                    <tbody>
                                        <tr>
                                            ${content}
                                        </tr>
                                    </tbody>
                                </table>
                             </td>
                        </tr>
                    </tbody>
                </table>
            `
        )
    }

}