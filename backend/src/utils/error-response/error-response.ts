import { ErrorType } from './error-response.types';
import { ResponseResultBase } from '../response-result/response-result.types';

export default class ErrorResponse extends Error {
  constructor(private _responseResult: ResponseResultBase, private _type: ErrorType) {
    super(_responseResult.message);
  }

  get responseResult() {
    return this._responseResult;
  }

  set responseResult(responseResult: ResponseResultBase) {
    this._responseResult = responseResult;
  }
  get type() {
    return this._type;
  }

  set type(type: ErrorType) {
    this._type = type;
  }

}
