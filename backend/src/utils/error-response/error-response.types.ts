export enum ErrorType {
  CART = 'CART',
  CATEGORY = 'CATEGORY',
  EMAIL = 'EMAIL',
  ORDER = 'ORDER',
  PAYMENT = 'PAYMENT',
  PRODUCT = 'PRODUCT',
  SERVER = 'SERVER',
}