
import {StatusCodes} from 'http-status-codes';

import {OperationStatus, ResponseResultBase, ResponseResultData} from './response-result.types';
export default class ResponseResultBuilder<T> {
    private message = '';
    private log = '';
    private status: number = StatusCodes.OK
    private operationStatus: OperationStatus = OperationStatus.SUCCESS;
    private data?: T;

    withMessage(message: string): ResponseResultBuilder<T> {
        this.message = message;
        return this;
    }

    withStatus(status: number): ResponseResultBuilder<T> {
        this.status = status;
        return this;
    }

    withOperationStatus(operationStatus: OperationStatus): ResponseResultBuilder<T> {
        this.operationStatus = operationStatus;
        return this;
    }

    withData(data: T): ResponseResultBuilder<T> {
        this.data = data;
        return this;
    }

    withLog(log: string): ResponseResultBuilder<T> {
        this.log = log;
        return this;
    }

    build(): ResponseResultBase | ResponseResultData<T> {
        return {
            message: this.message,
            status: this.status,
            operationStatus: this.operationStatus,
            data: this.data
        };
    }
}
