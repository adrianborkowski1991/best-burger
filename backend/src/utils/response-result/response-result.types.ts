export enum OperationStatus {
  ERROR='ERROR',
  SUCCESS='SUCCESS'
}
export interface ResponseResultBase {
  message: string;
  status: number;
  operationStatus: OperationStatus;
  log?: string;
}
export interface ResponseResultData<T> extends ResponseResultBase{
  data?: T | T[];
}