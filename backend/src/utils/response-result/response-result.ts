import {StatusCodes} from 'http-status-codes';

import { ResponseResultBase, ResponseResultData, OperationStatus } from './response-result.types';
import ResponseResultBuilder from './response-result-builder';
export default class ResponseResult {
  static serverUnavailable(log?: string): ResponseResultBase {
    return new ResponseResultBuilder()
        .withMessage('The server is currently experiencing some issues.')
        .withLog(log ?? 'The server is currently experiencing some issues.')
        .withStatus(StatusCodes.INTERNAL_SERVER_ERROR)
        .withOperationStatus(OperationStatus.ERROR)
        .build();
  }


  static unprocessableEntity(addedMessage?: string, log?: string): ResponseResultBase {
    return new ResponseResultBuilder()
        .withMessage(`The request couldn't be processed due to invalid data. ${addedMessage ?? ''}`)
        .withLog(log ?? `The request couldn't be processed due to invalid data. ${addedMessage ?? ''}`)
        .withStatus(StatusCodes.UNPROCESSABLE_ENTITY)
        .withOperationStatus(OperationStatus.ERROR)
        .build();
  }

  static notFound(addedMessage?: string, log?: string): ResponseResultBase {
    return new ResponseResultBuilder()
        .withMessage(`The requested resource was not found. ${addedMessage ?? ''}`)
        .withLog(log ?? `The requested resource was not found. ${addedMessage ?? ''}`)
        .withStatus(StatusCodes.NOT_FOUND)
        .withOperationStatus(OperationStatus.ERROR)
        .build();
  }

  static successGet<T>(data: T, log?: string): ResponseResultData<T> {
    return new ResponseResultBuilder<T>()
        .withMessage('Operation completed successfully.')
        .withLog(log ?? 'Operation completed successfully.')
        .withStatus(StatusCodes.OK)
        .withOperationStatus(OperationStatus.SUCCESS)
        .withData(data)
        .build();
  }
}