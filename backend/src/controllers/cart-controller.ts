import {Request, Response} from 'express';

import withServerException from './with-server-exception';
import { OrderProductsInstance} from '../models/order-products-model';
import {serviceController} from '../services/services-controller';
import ResponseResult from '../utils/response-result/response-result';
const readOrCreateCart = async (req: Request) => {
    const {cartService} = serviceController;

    if(!req.session.cart){
        const cart = await cartService.createCart();

        if(cart && cart.cart.id){
            req.session.cart = { id: cart.cart.id }
        }
        return cart;
    }
    else {
        return await cartService.readCartProducts(req.session.cart.id);
    }
};

export const getCart =  withServerException(async (req: Request, res: Response) => {
    const cartPack = await readOrCreateCart(req);
    const {cartProducts} = cartPack;

    res.json(ResponseResult.successGet(cartProducts));
});

export const putCartAddProduct =  withServerException(async (req: Request, res: Response) => {
    const {cartService} = serviceController;

    const cartPack = await readOrCreateCart(req);
    const cartProductToAdd = req.body as OrderProductsInstance;
    cartProductToAdd.cartId = cartPack.cart.id;
    const cartProducts = await cartService.addProductToCart(Number(cartPack.cart.id), cartProductToAdd, cartPack.cartProducts)

    res.json(ResponseResult.successGet(cartProducts));
});

export const putCartRemoveProduct =  withServerException(async (req: Request, res: Response) => {
    const {cartService} = serviceController;

    const cartPack = await readOrCreateCart(req);
    const cartProducts = await cartService.deleteProductFromCart(Number(cartPack.cart.id), req.body.productId, cartPack.cartProducts)

    res.json(ResponseResult.successGet(cartProducts));
});
