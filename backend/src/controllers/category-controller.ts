import {Request, Response} from 'express';

import withServerException from './with-server-exception';
import {CategoryInstance} from '../models/category-model';
import {serviceController} from '../services/services-controller';
import ResponseResult from '../utils/response-result/response-result';

export const getCategories =  withServerException(async (req: Request, res: Response) => {
    const { categoryService}  = serviceController;

    const categories : CategoryInstance[] = await categoryService.readCategoriesAll();

    return res.json(ResponseResult.successGet(categories))
});