import * as dotenv from 'dotenv';
import {Request, Response} from 'express';

import withServerException from './with-server-exception';
import {OrderAttributes} from '../models/order-model';
import {serviceController} from '../services/services-controller';
import {OrderEmailTemplateHelper} from '../utils/email/order-email-template-helper';
import ErrorResponse from '../utils/error-response/error-response';
import {ErrorType} from '../utils/error-response/error-response.types';
import ResponseResult from '../utils/response-result/response-result';

dotenv.config();

export const postOrder =  withServerException(async (req: Request, res: Response) => {
    const {cartService, paymentService, orderService, emailService, productService} = serviceController;

    const orderBody = req.body as OrderAttributes;
    const cartId = req.session.cart?.id;

    if (!cartId || !orderBody) {
        throw new ErrorResponse(ResponseResult.unprocessableEntity('Invalid cart or order data.'), ErrorType.ORDER);
    }

    const cartPack = await cartService.readCartProducts(cartId);
    const orderId = await orderService.createOrder(cartPack, orderBody);

    if (!orderId ) {
        throw new ErrorResponse(ResponseResult.serverUnavailable('Order isn\'t created.'), ErrorType.ORDER);
    }

    const paymentUrl = await paymentService.generatePaymentLink(cartPack.cartProducts, orderBody.email, orderId);
    await cartService.deactivateCart(cartId);

    req.session.destroy((error) => {
        if(error){
            throw new ErrorResponse(ResponseResult.serverUnavailable('There was an error while destroying session.'), ErrorType.SERVER);
        }
    });

    const products = await productService.readProductsByIds(cartPack.cartProducts.map(element=> element.productId));
    const contentHTML = await OrderEmailTemplateHelper.orderConfirmation(cartPack.cartProducts, products, orderId)
    await emailService.sendEmail(orderBody.email, `Hi ${orderBody.fullName}! Order Confirmation. Your Order Has Been Accepted`, contentHTML)

    res.json(ResponseResult.successGet(paymentUrl));
});

export const putOrderPaid =  withServerException(async (req: Request, res: Response) => {
    const {emailService, orderService} = serviceController;

    const orderId = +req.params.id;
    const order = await orderService.paidOrder(orderId);
    const contentHTML = await OrderEmailTemplateHelper.orderPaid(orderId);
    await emailService.sendEmail(order.email, `Hi ${order.fullName}! Thank you for your payment!`, contentHTML)

    res.json(ResponseResult.successGet('The order status has been changed to paid.'));
});
