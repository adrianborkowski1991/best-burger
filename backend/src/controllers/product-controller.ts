import {Request, Response} from 'express';

import withServerException from './with-server-exception';
import {ProductInstance} from '../models/product-model';
import {serviceController} from '../services/services-controller';
import ResponseResult from '../utils/response-result/response-result';

export const getProducts =  withServerException(async (req: Request, res: Response) => {
    const {filter} = req.params;
    const { categoryService, productService}  = serviceController;

    let products : ProductInstance[] = [];

    if(filter === 'news'){
        products = await productService.readProductsByNewAttribute(true);
    }
    else {
        const category  = await categoryService.readCategoryByName(filter);
        products = await productService.readProductsByCategoryId(Number(category.id));
    }

    return res.json(ResponseResult.successGet(products))
});