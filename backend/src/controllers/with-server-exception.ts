import {NextFunction, Request, Response} from 'express';

const withErrorHandling  = <T>(fn: (req: Request, res: Response, next: NextFunction) => Promise<T>) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        try {
            await fn(req, res, next);
        } catch (err) {
            next(err);
        }
    };
};

export default withErrorHandling