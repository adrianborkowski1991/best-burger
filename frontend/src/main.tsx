import ReactDOM from 'react-dom/client';
import {Provider} from 'react-redux';

import {App} from './components/app/app';
import {store} from './store/configure-store.ts';

ReactDOM.createRoot(document.getElementById('root')!).render(
   <Provider store={store}>
     <App />
   </Provider>,
)
