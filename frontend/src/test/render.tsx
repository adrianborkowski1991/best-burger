import {BrowserRouter} from 'react-router-dom';
import {FC, PropsWithChildren} from 'react';
import {Provider} from 'react-redux';
import {render} from '@testing-library/react';
import {ThemeProvider} from 'styled-components';

import {store} from '../store/configure-store.ts';
import {theme} from '../style/theme.tsx';

const Wrapper : FC<PropsWithChildren> = ({ children }) => {
    return (<ThemeProvider theme={theme}>
        <BrowserRouter>
            <Provider store={store}>{children}</Provider>
        </BrowserRouter>
    </ThemeProvider>)
}

export function renderWithProviders(ui: React.ReactElement) {
    return render(<Wrapper>{ui}</Wrapper> )
}