import MockAdapter from 'axios-mock-adapter';

import {APIPath, OperationStatus} from '../../../store/fetch-utils/fetch-utils.types.ts';
import {axiosConfig} from '../../../store/fetch-utils/fetch-utils.ts';

const getCartResponse = {
    message: "Operation completed successfully.",
    status: 200,
    operationStatus: OperationStatus.SUCCESS,
    data: [
        {
            id: 2,
            orderId: null,
            quantity: 1,
            cartId: 5,
            productId: 2,
            title: "Burger Beef",
            price: 22.9,
            imageUrl: "/public/product-burger-beef.png",
            new: false,
            categoryId: 2
        },
        {
            id: 4,
            quantity: 2,
            productId: 4,
            cartId: 5,
            orderId: null,
            title: "Burger chorizo",
            price: 27.9,
            imageUrl: "/public/product-burger-chorizo.png",
            new: true,
            categoryId: 2
        },
        {
            id: 1,
            orderId: null,
            quantity: 3,
            cartId: 5,
            productId: 1,
            title: "Cheeseburger",
            price: 22.9,
            imageUrl: "/public/product-cheeseburger.png",
            new: false,
            categoryId: 2
        }
    ]
};

export const mockCartResponse = () => {
    const mock = new MockAdapter(axiosConfig)
    mock.onGet(APIPath.CART_ITEMS).reply(200, getCartResponse)
}