import {AppDispatch, store} from '../../../store/configure-store.ts';
export const dispatchApp = store.dispatch as AppDispatch;
