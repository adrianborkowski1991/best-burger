// src/test/mocks/fileMock.js

// eslint-disable-next-line no-undef
module.exports = {
    __esModule: true,
    default: 'test-file-stub',
    moduleNameMapper: {
        "/.(gif|ttf|eot|svg|png)$/": "...\frontend\test__ mocks __\fileMock.js"
    },

};