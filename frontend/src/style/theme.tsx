
export interface Colors{
  black: string;
  white: string;
  brown: string;
  pearl: string;
  antique: string;
  orange: string;
  red: string;
}

export interface Breakpoints{
  desktopUp: number;
  desktopLarge: number;
  desktopSmall: number;
  tablet: number;
  mobileLarge: number;
  mobileSmall: number;
}

export interface Fonts{
  primary: string;
  secondary: string;
}

export interface Theme{
  colors: Colors;
  breakpoints: Breakpoints;
  fonts: Fonts;
}

export const breakpoints : Breakpoints = {
  desktopUp: 1920,
  desktopLarge: 1440,
  desktopSmall: 1024,
  tablet: 768,
  mobileLarge: 568,
  mobileSmall: 320,
}

export const theme: Theme = {
  colors: {
    black: '#000000',
    white: '#FFFFFF',
    brown: '#522118',
    pearl: '#e8dcc9',
    antique: '#f5ebdc',
    orange: '#fd8733',
    red: '#d62416'
  },
  breakpoints: breakpoints,
  fonts: {
    primary: "'Oswald', sans-serif",
    secondary: "'Lobster', cursive",
  }
};
