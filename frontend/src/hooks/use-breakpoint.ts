import {useEffect, useState} from 'react';

import {Breakpoints, breakpoints} from '../style/theme.tsx';

const useBreakpoint = () => {
    const [breakpoint, setBreakpoint] = useState<keyof typeof breakpoints | null>(null);

    const breakpointCheck = (breakpointToCheck: keyof typeof breakpoints) => {
        if (!breakpoint) {
            return false;
        }

        return breakpoints[breakpoint] <= breakpoints[breakpointToCheck];
    };

    useEffect(() => {
        const handleResize = () => {
            const width = window.innerWidth;

            for (const key in breakpoints) {
                const convertKey = key as keyof Breakpoints;

                if (width >= breakpoints[convertKey]) {
                    setBreakpoint(convertKey);
                    break;
                }
            }
        };

        window.addEventListener('resize', handleResize);

        handleResize();

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    return { breakpoint, breakpointCheck };
};

export default useBreakpoint;