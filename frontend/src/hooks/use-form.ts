import {useCallback, useEffect, useState} from 'react';

import {InputConfig, InputState, InputType} from '../types/form.ts';

function changeInput<T extends Record<keyof T, InputType>>(inputs: InputState<T>[], input: InputState<T>){
   const inputsFiltered = inputs.filter(inputNew => inputNew.name !== input.name);
   inputsFiltered.push(input);

   return inputsFiltered;
}

export const useForm = <T extends Record<keyof T, InputType>>(inputsConfig: InputConfig<T>[]) => {
   const [inputs, setInputs] = useState<InputState<T>[]>([]);

   const checkError = useCallback(
       (input: InputConfig<T>, valueParam: InputType) => {
          const inputGet = inputs.find(inputFiltered => input.name === inputFiltered.name);
          if (inputGet && inputGet.isValid) {
             const checkIsValid = inputGet.isValid(valueParam);

             return !checkIsValid;
          }

          return false;
       },
       [inputs]
   );

   const initializeInputState = useCallback(() => {
      const newInputs = inputsConfig.map(inputConfig => {

         return {
            ...inputConfig,
            isError: false,
            isTouch: false,
            value: ''
         };
      });
      setInputs(newInputs);
   }, [inputsConfig])

   useEffect(() => {
      if (inputs.length === 0) {
         initializeInputState();
      }
   }, [initializeInputState, inputs]);

   const onChange = (event : React.ChangeEvent<HTMLInputElement>) => {
      const valueTarget = event.target.value;
      const name = event.target.name;

      const inputGet = inputs.find(inputFiltered => name === inputFiltered.name);

      if (inputGet) {
         inputGet.isTouch = true;
         inputGet.value = valueTarget;
         inputGet.isError = checkError(inputGet, valueTarget);

         setInputs(prevState => changeInput(prevState, inputGet));
      }
   };

   const onSubmit = (inputGet: InputState<T>) => {
      inputGet.isTouch = false;

      inputGet.isError = checkError(inputGet, inputGet.value);
      setInputs(prevState => changeInput(prevState, inputGet));

      return inputGet;
   };

   const getObjectFromInputs = <T extends Record<keyof T, InputType>>(
       inputs: InputState<T>[]
   ): T => {
      const result = {} as T;

      inputs.forEach((input) => {
         const key = input.name as keyof T;
         result[key] = input.value as T[keyof T];
      });

      return result;
   };

   const onSubmitForm = () => {
      const newInputsSubmitAll = inputs.map(inputElement => {
         return onSubmit(inputElement);
      });

      let countErrors = 0;
      newInputsSubmitAll.forEach(inputElement => {
         countErrors += inputElement.isError ? 1 : 0;
      });

      if(countErrors === 0){
         return getObjectFromInputs(inputs);
      }
      return false;
   };

   const clearAll = () => {
      initializeInputState();
   };

   const getValue = (name: keyof T) => {
      const input = inputs.find(inputFiltered => name === inputFiltered.name);

      return input ? input.value  : '';
   };

   const getError = (name: keyof T) => {
      const input = inputs.find(inputFiltered => name === inputFiltered.name);

      return input ? input.isError : false;
   };

   return {
      inputs,
      onChange,
      clearAll,
      getValue,
      getError,
      onSubmitForm
   };
};
