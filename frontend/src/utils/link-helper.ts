import {Links} from '../components/app/app.helper.tsx';
import {APIPath} from '../store/fetch-utils/fetch-utils.types.ts';

export const generateLink = (link: Links | APIPath, params: [string, string][] ) => {
    let newLink : string = link;
    params.forEach(param=> {
        newLink = newLink.replace(param[0], param[1]);
    })
    return newLink;
}