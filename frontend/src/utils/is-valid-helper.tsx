import {InputType, ValidatorFunction} from '../types/form.ts';

export enum Validator{
    NOT_EMPTY,
    PHONE,
    EMAIL,
}

const notEmptyValidator : ValidatorFunction = (value: InputType) => String(value).length > 0;
const emailValidator : ValidatorFunction = (value: InputType) => /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(String(value));
const phoneValidator : ValidatorFunction = (value: InputType) => /^\d{9}$/.test(String(value));

export const functionValidatorGenerator = (validator: Validator) => {
    switch (validator){
        case Validator.NOT_EMPTY: return notEmptyValidator;
        case Validator.PHONE: return phoneValidator;
        case Validator.EMAIL: return emailValidator;
    }
}