import {css} from 'styled-components';

export const priceCommon = css`
  grid-area: price;
  display: flex;
  gap: 5px;

  @media (max-width: ${({theme}) => theme.breakpoints.mobileLarge}px) {
    justify-content: end;
  }
`