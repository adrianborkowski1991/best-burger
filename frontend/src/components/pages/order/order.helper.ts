import {Order} from './order.types';
import {InputConfig} from '../../../types/form';
import {functionValidatorGenerator, Validator} from '../../../utils/is-valid-helper';

export enum LoginField {
    ADDRESS = 'address',
    NAME = 'fullName',
    EMAIL = 'email',
    PHONE = 'phone',
}

export const inputOrderConfig : InputConfig<Order>[] = [
    {
        name: LoginField.ADDRESS,
        isValid: functionValidatorGenerator(Validator.NOT_EMPTY),
        isRequired: true,
        placeholder: 'Address',
        type: 'text',
        errorMessage: 'Field address cannot be empty.'
    },
    {
        name: LoginField.NAME,
        isValid: functionValidatorGenerator(Validator.NOT_EMPTY),
        isRequired: true,
        placeholder: 'Name',
        type: 'text',
        errorMessage: 'Field name cannot be empty.'
    },
    {
        name: LoginField.EMAIL,
        isValid: functionValidatorGenerator(Validator.EMAIL),
        isRequired: true,
        placeholder: 'Email',
        type: 'text',
        errorMessage: `Invalid email value.`
    },
    {
        name: LoginField.PHONE,
        isValid: functionValidatorGenerator(Validator.PHONE),
        isRequired: true,
        placeholder: 'Phone',
        type: 'text',
        errorMessage: `Invalid phone number value.`
    }
]