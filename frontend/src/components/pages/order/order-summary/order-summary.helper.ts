import {orderConfig} from '../../../../config/order-config.ts';
import {CartItem} from '../../../../store/slices/cart/cart-slice.types.ts';

export default class OrderSummaryHelper {
    constructor(private cartItems: CartItem[] | null, private deliveryPrice : number = orderConfig.delivery ) {}

    public calculateTotalPrice = () => {
        return this.cartItems ? this.cartItems.reduce(
            (accumulator, currentValue) => accumulator + (currentValue.price * currentValue.quantity), 0) : 0;
    }

    public calculateTotalPriceWithDelivery = () => {
        return this.calculateTotalPrice() + this.deliveryPrice;
    }

    public getDeliveryPrice = () => {
        return this.deliveryPrice;
    }
}