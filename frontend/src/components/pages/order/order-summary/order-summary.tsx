import {useSelector} from 'react-redux';

import {SummaryList} from './order-summary.styled.tsx';
import OrderItem from './summary-item/summary-item.tsx';
import OrderSummaryHelper from './order-summary.helper.ts';
import {OrderColumn, OrderColumnHeader} from '../order.styled';
import Text from '../../../common/text/text';
import {TextSize, TextStyle, TextWeight} from '../../../common/text/text.types';
import {RootState} from '../../../../store/root-reducer.ts';

const OrderSummary = () => {
  const cartItemsList = useSelector((state: RootState) => state.cartItems.list);

  const orderSummaryHelper = new OrderSummaryHelper(cartItemsList);

  const summaryItems = [
    {
      label: 'Products',
      price: orderSummaryHelper.calculateTotalPrice()
    },
    {
      label: 'Delivery',
      price: orderSummaryHelper.getDeliveryPrice()
    },
    {
      label: 'Total',
      price: orderSummaryHelper.calculateTotalPriceWithDelivery()
    },
  ];

  return (<OrderColumn>
    <OrderColumnHeader>
      <Text size={TextSize.XL} typeElement={'h1'} weight={TextWeight.BOLD} textStyle={TextStyle.ITALIC} font={'secondary'} decoration={'underline'}>Summary</Text>
    </OrderColumnHeader>
    <SummaryList>
      {summaryItems.map((item) => <OrderItem key={item.label} {...item} />)}
    </SummaryList>
  </OrderColumn>)
}

export default OrderSummary;