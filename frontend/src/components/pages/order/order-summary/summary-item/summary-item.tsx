import {FC} from 'react';

import {SummaryItemProps} from './summary-item.types.ts';
import {SummaryItem} from '../order-summary.styled.tsx';
import Text from '../../../../common/text/text.tsx';
import {TextSize, TextWeight} from '../../../../common/text/text.types.ts';
import Price from '../../../../common/price/price.tsx';

const OrderItem : FC<SummaryItemProps> = ({label, price}) => {
    return (
        <SummaryItem>
            <Text weight={TextWeight.BOLD} size={TextSize.L}>{label}</Text>
            <Price price={price} integerPartSize={TextSize.L} fractionalPartSize={TextSize.S}/>
        </SummaryItem>
    )
}

export default OrderItem;