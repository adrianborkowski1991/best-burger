import styled from 'styled-components';

export const SummaryList = styled.ul`

`

export const SummaryItem = styled.li`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 50px;
  border-bottom: 1px solid #e8dcc9;

`