import {useEffect} from 'react';
import {useNavigate} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';

import OrderForm from './order-form/order-form';
import OrderSummary from './order-summary/order-summary';
import {inputOrderConfig} from './order.helper.ts';
import {Order} from './order.types.ts';
import {OrderButtonContainer, OrderStyled} from './order.styled.tsx';
import {Links} from '../../app/app.helper.tsx';
import Button from '../../common/button/button';
import Loader from '../../common/loader/loader.tsx';
import Layout from '../../layout/layout/layout.tsx';
import {useForm} from '../../../hooks/use-form.ts';
import {AppDispatch} from '../../../store/configure-store.ts';
import {postOrderAsync} from '../../../store/slices/order/order-slice.ts';
import {RequestState} from '../../../store/slice-utils/slices-utils.types.ts';
import {RootState} from '../../../store/root-reducer.ts';

const Order = () => {
  const navigate = useNavigate();
  const {onChange, getValue, getError, onSubmitForm} = useForm<Order>(inputOrderConfig);
  const dispatch = useDispatch<AppDispatch>();
  const cartItemsList = useSelector((state: RootState) => state.cartItems.list);
  const orderLink = useSelector((state: RootState) => state.orderSlice.view);
  const orderState = useSelector((state: RootState) => state.orderSlice.state?.post);

  useEffect(()=> {
    if(cartItemsList?.length === 0){
      navigate(Links.HOME);
    }
  }, [cartItemsList?.length, navigate])

  useEffect(()=> {
    if(orderLink){
      window.location.href = orderLink;
    }
  }, [navigate, orderLink])

  const handleSubmitOrder = async () => {
    const submitForm = onSubmitForm();

    if(submitForm){
      await dispatch(postOrderAsync(submitForm));
    }
  }

  return (<Layout elements={[['Order']]}>
    {orderState === RequestState.LOADING && <Loader />}
    <OrderStyled>
      <OrderForm inputOrderConfig={inputOrderConfig} onChange={onChange} getValue={getValue} getError={getError} />
      <OrderSummary/>
    </OrderStyled>
    <OrderButtonContainer>
      <Button color={'red'} full={String(true)} handleButtonClick={handleSubmitOrder}>Order & Pay</Button>
    </OrderButtonContainer>
  </Layout>)
}

export default Order;