import styled from 'styled-components';

export const OrderStyled = styled.div`
  display: flex;
  gap: 48px;

  @media (max-width: ${({theme}) => theme.breakpoints.mobileLarge}px) {
    flex-direction: column;
    gap: 20px;
  }
`

export const OrderColumn = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  
  @media (max-width: ${({theme}) => theme.breakpoints.mobileLarge}px) {
    width: 100%;
  }
`

export const OrderColumnHeader = styled.div`
  margin-bottom: 32px;
`

export const OrderButtonContainer = styled.div`
  width: 50%;
  margin: 24px auto 0 auto;
  
  @media (max-width: ${({theme}) => theme.breakpoints.mobileLarge}px) {
    width: 100%;
  }
`