import {FC} from 'react';


import {OrderColumn, OrderColumnHeader} from '../order.styled';
import {OrderFormProps} from './order-form.types';
import Input from '../../../common/input/input';
import Text from '../../../common/text/text';
import {TextSize, TextStyle, TextWeight} from '../../../common/text/text.types';

const OrderForm : FC<OrderFormProps>= ({inputOrderConfig, onChange, getValue, getError}) => {

  return (<OrderColumn>
    <OrderColumnHeader>
      <Text size={TextSize.XL} typeElement={'h1'} weight={TextWeight.BOLD} textStyle={TextStyle.ITALIC} font={'secondary'} decoration={'underline'}>Your data</Text>
    </OrderColumnHeader>
    {
      inputOrderConfig.map(input=>
        <Input
          key={input.name}
          placeholder={input.placeholder}
          name={input.name}
          type={input.type}
          value={getValue(input.name)}
          error={getError(input.name)}
          errorMessage={input.errorMessage}
          onChange={onChange}
        />
      )
    }
  </OrderColumn>)
}

export default OrderForm;