import {Order} from '../order.types';
import {InputConfig, InputType} from '../../../../types/form';

export interface OrderFormProps {
  inputOrderConfig : InputConfig<Order>[];
  onChange: (event : React.ChangeEvent<HTMLInputElement>) => void;
  getValue: (name: keyof Order) => InputType;
  getError: (name:  keyof Order) => boolean;
}