export interface Order {
    address: string;
    fullName: string;
    email: string;
    phone: string;
}
