import Layout from '../../layout/layout/layout.tsx';

const NotFound = () => {
  return <Layout elements={[['Not found']]}>Not found</Layout>
}

export default NotFound;