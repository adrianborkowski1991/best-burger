import {FC, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import {convertProductToCartItem} from './product-item.helper.ts';
import {AddToCartButton, Image, Title} from './product-item.styled.tsx';
import {ProductItemProps} from './product-item.types.ts';
import {ProductItemStyled} from '../products.styled.tsx';
import Button from '../../../common/button/button.tsx';
import Price from '../../../common/price/price.tsx';
import Text from '../../../common/text/text.tsx';
import {TextSize, TextWeight} from '../../../common/text/text.types.ts';
import CartModal from '../../../layout/cart-modal/cart-modal.tsx';
import {AppDispatch} from '../../../../store/configure-store.ts';
import {RootState} from '../../../../store/root-reducer.ts';
import {putAddItemToCartAsync} from '../../../../store/slices/cart/cart-slice.ts';

const ProductItem : FC<ProductItemProps> = ({product}) => {
    const [cartOpen, setCartOpen] = useState(false);
    const dispatch = useDispatch<AppDispatch>();
    const cartItemsList  = useSelector((state: RootState) => state.cartItems.list);

    const handleButtonClick = () => {
        if(product.id){
            const addedItem = cartItemsList?.find(item => item.productId === product.id);

            void dispatch(putAddItemToCartAsync({
                quantity: addedItem ? (addedItem.quantity + 1) : 1,
                cartItem: convertProductToCartItem(product),
            }));
        }

        setCartOpen(true);
    }

    const handleCloseModal = (event: React.MouseEvent<HTMLDivElement | HTMLButtonElement>) => {
        event.stopPropagation();
        setCartOpen(false);
    };

    return (<ProductItemStyled>
        <Title><Text typeElement={'h1'} size={TextSize.XL} weight={TextWeight.BOLD} color={'brown'} font={'secondary'}>{product.title}</Text></Title>
        <Image src={`${import.meta.env.VITE_SERVER_URL}${product.imageUrl}`}/>
        <Price price={product.price} integerPartSize={TextSize.XXL} fractionalPartSize={TextSize.XL}/>
        <AddToCartButton><Button handleButtonClick={handleButtonClick}>Add to cart</Button></AddToCartButton>
        {cartOpen && <CartModal handleCloseModal={handleCloseModal}/>}
    </ProductItemStyled>)
}

export default ProductItem;