import {Product} from '../../../../store/slices/product/product-slice.types.ts';

export interface ProductItemProps {
    product: Product;
}