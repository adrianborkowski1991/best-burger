import styled, {css} from 'styled-components';

import {priceCommon} from '../../../../utils/common-styled-helper.tsx';

const titleCommon = css`
  grid-area: title;
  text-align: right;

  @media (max-width: ${({theme}) => theme.breakpoints.mobileLarge}px) {
    text-align: left;
  }
`

export const Title = styled.div`
  ${titleCommon}
`

export const TitleSkeleton = styled.div`
  width: 150px;
  margin-left: auto;
  margin-bottom: 10px;
  ${titleCommon}
`

const imageCommon = css`
  grid-area: image;
  z-index: 10;
  width: 200px;
  position: absolute;

  @media (max-width: ${({theme}) => theme.breakpoints.mobileLarge}px) {
    position: relative;
    top: 0;
    left: 50%;
    transform: translate(-50%);
  }
`

export const Image = styled.img`
  top: -60px;
  right: -25px;
  ${imageCommon}
`

export const ImageSkeleton = styled.div`
  height: 100px;
  right: 0;
  
  @media (max-width: ${({theme}) => theme.breakpoints.mobileLarge}px) {
    margin: 50px 0;
    height: 160px;
  }
  
  ${imageCommon}
`

export const PriceSkeleton = styled.div`
  width: 80px;
  height: 65px;

  @media (max-width: ${({theme}) => theme.breakpoints.mobileLarge}px) {
    position: absolute;
    right: 0;
    top: 0;
  }

  ${priceCommon}
`

const addToCartButtonCommon = css`
  grid-area: button;
  display: flex;
  justify-content: start;
  align-items: end;
`
export const AddToCartButton = styled.div`
  ${addToCartButtonCommon}
`
export const AddToCartButtonSkeleton = styled.div`
  width: 107px;
  height: 44px;
  position: absolute;
  bottom: 0;
  
  @media (max-width: ${({theme}) => theme.breakpoints.mobileLarge}px) {
    width: 100%;
  }
  
  ${addToCartButtonCommon}
`

export const SkeletonElement = styled.div`
  border-radius: 20px;
  width: 100%;
  height: 100%;
  ${({theme}) => `background-color: ${theme.colors.pearl};`};
`