import {CartItem} from '../../../../store/slices/cart/cart-slice.types.ts';
import {Product} from '../../../../store/slices/product/product-slice.types.ts';

export const convertProductToCartItem = (product: Product): CartItem => {
    return {
        quantity: 1,
        productId: product.id ?? 0,
        title: product.title,
        price: product.price,
        imageUrl: product.imageUrl,
    }
}