export const convertPathnameToBreadcrumbsElement = (pathname: string) => {
    const locationConvert = pathname.replace('/', '');
    return  locationConvert.charAt(0).toUpperCase() + locationConvert.substring(1);
}
