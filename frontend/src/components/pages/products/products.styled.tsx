import styled from 'styled-components';

export const ProductItemStyled = styled.article`
  display: grid;
  grid-template-columns: 50% 50%;
  grid-template-rows: 50% 50%;
  grid-template-areas:
    "price title"
    "button image";
  border-radius: 20px;
  flex-direction: column;
  justify-content: center;
  padding: 20px;
  height: 175px;
  position: relative;
  
  &:not(:last-child){
    margin-bottom: 20px;
  }
  
  ${({theme}) => 
      `
        border: 2px solid ${theme.colors.pearl};
      `
  };

  @media (max-width: ${({theme}) => theme.breakpoints.mobileLarge}px) {
    grid-template-columns: 50% 50%;
    grid-template-rows: 20% 60% 20%;
    grid-template-areas:
    "title price"
    "image image"
    "button button";
    height: auto;
  }
`