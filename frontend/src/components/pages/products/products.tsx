import {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useLocation} from 'react-router-dom';

import ProductItem from './product-item/product-item.tsx';
import ProductsSkeleton from './products-skeleton/products-skeleton.tsx';
import {convertPathnameToBreadcrumbsElement} from './products.helper.ts';
import Layout from '../../layout/layout/layout.tsx';
import {AppDispatch} from '../../../store/configure-store.ts';
import {getAllProductAsync} from '../../../store/slices/product/product-slice.ts';
import {RootState} from '../../../store/root-reducer.ts';

const Products = () => {
  const [breadcrumbsElement, setBreadcrumbsElement] = useState('');
  const location = useLocation();
  const dispatch = useDispatch<AppDispatch>();
  const productList = useSelector((state: RootState) => state.product.list);

  useEffect(()=> {
    const convertPathname = convertPathnameToBreadcrumbsElement(location.pathname);
    setBreadcrumbsElement(convertPathname)
  }, [location.pathname])

  useEffect(()=> {
    void dispatch(getAllProductAsync(location.pathname));
  }, [location.pathname, dispatch])

  return <Layout elements={[[breadcrumbsElement]]}>
    {productList ? productList.map(product => <ProductItem key={product.id} product={product}/>) : <ProductsSkeleton/>}
  </Layout>
}

export default Products;