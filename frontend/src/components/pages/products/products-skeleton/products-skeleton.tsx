import {ProductItemStyled} from '../products.styled.tsx';
import {
    AddToCartButtonSkeleton,
    ImageSkeleton,
    PriceSkeleton,
    SkeletonElement,
    TitleSkeleton
} from '../product-item/product-item.styled.tsx';

const ProductsSkeleton = () => {

    return <>{Array.from({ length: 3 }).map((_, index) => (
        <ProductItemStyled key={index}>
            <TitleSkeleton><SkeletonElement/></TitleSkeleton>
            <ImageSkeleton><SkeletonElement/></ImageSkeleton>
            <PriceSkeleton><SkeletonElement/></PriceSkeleton>
            <AddToCartButtonSkeleton><SkeletonElement/></AddToCartButtonSkeleton>
        </ProductItemStyled>
    ))}</>;
}

export default ProductsSkeleton;