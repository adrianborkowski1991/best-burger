import {useEffect} from 'react';
import {useNavigate, useSearchParams} from 'react-router-dom';
import {useDispatch} from 'react-redux';

import {Links} from '../../app/app.helper.tsx';
import Loader from '../../common/loader/loader.tsx';
import Layout from '../../layout/layout/layout.tsx';
import {AppDispatch} from '../../../store/configure-store.ts';
import {putOrderPaidAsync} from '../../../store/slices/order/order-slice.ts';
import {generateLink} from '../../../utils/link-helper.ts';

const Home = () => {
    const navigate = useNavigate();
    const [search] = useSearchParams();
    const dispatch = useDispatch<AppDispatch>();

    useEffect(()=> {
        const orderIdParam = search.get('orderId');
        if(orderIdParam){
            dispatch(putOrderPaidAsync(orderIdParam));
        }
    }, [dispatch, search])

    useEffect(()=> {
        navigate(generateLink(Links.PRODUCTS, [[':slug', 'news']]))
    }, [navigate])

    return <Layout elements={[['News']]}><Loader/></Layout>
}

export default Home;