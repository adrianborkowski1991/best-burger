import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import {links} from './app.helper';
import Loader from '../common/loader/loader';

export const App = () => {

  return (<RouterProvider router={createBrowserRouter(links)} fallbackElement={<Loader />} />)
}