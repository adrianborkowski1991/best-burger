import Home from '../pages/home/home.tsx';
import NotFound from '../pages/notFound/not-found';
import Order from '../pages/order/order';
import Products from '../pages/products/products';

export enum Links {
  HOME = '/',
  PRODUCTS = '/:slug',
  ORDER = '/order',
  NOT_FOUND = '*'
}

export const links = [
  {
    path: Links.HOME,
    element: <Home/>
  },
  {
    path: Links.PRODUCTS,
    element: <Products />
  },
  {
    path: Links.ORDER,
    element: <Order />
  },
  {
    path: Links.NOT_FOUND,
    element: <NotFound/>
  }
];
