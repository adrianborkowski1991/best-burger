import {FooterLink, FooterStyled} from './footer.styled.tsx';
import Container from '../../common/container/container';
import Text from '../../common/text/text.tsx';

const Footer = () => {
  return (<Container typeElement={'footer'}>
    <FooterStyled>
      <FooterLink href={'https://adrianborkowski.dev'} target={'_blank'}><Text>Implemented by: Adrian Borkowski</Text></FooterLink>
      <FooterLink href={'https://gitlab.com/users/adrianborkowski1991/projects'} target={'_blank'}>To Repository 💡</FooterLink>
  </FooterStyled>
  </Container>)
}

export default Footer;