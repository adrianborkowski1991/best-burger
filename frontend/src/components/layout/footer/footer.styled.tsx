import styled from 'styled-components';

export const FooterStyled = styled.div`
  display: flex;
  padding: 20px 0;
  justify-content: space-between;
  align-items: center;
`

export const FooterLink = styled.a`
  display: flex;
  gap: 5px;
`
