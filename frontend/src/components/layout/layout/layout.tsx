import {FC, useEffect} from 'react';
import {ThemeProvider} from 'styled-components';
import {useDispatch, useSelector} from 'react-redux';

import {Wrapper} from './layout.styled.tsx';
import {LayoutProps} from './layout.types.ts';
import Breadcrumbs from '../header/breadcrumbs/breadcrumbs.tsx';
import Header from '../header/header.tsx';
import Footer from '../footer/footer.tsx';
import Container from '../../common/container/container.tsx';
import {getAllCartItemsAsync} from '../../../store/slices/cart/cart-slice.ts';
import {AppDispatch} from '../../../store/configure-store.ts';
import {RootState} from '../../../store/root-reducer.ts';
import {GlobalStyle} from '../../../style/global.ts';
import {theme} from '../../../style/theme.tsx';

const Layout: FC<LayoutProps>= ({children, elements}) => {
  const dispatch = useDispatch<AppDispatch>();
  const cartItems = useSelector((state: RootState) => state.cartItems.list);

  useEffect(()=> {
    if(!cartItems){
      void dispatch(getAllCartItemsAsync());
    }
  }, [cartItems, dispatch])
  
  return <ThemeProvider theme={theme}>
    <GlobalStyle />
    <Header/>
    <Breadcrumbs elements={elements}/>
    <main>
      <Container variant={'antique'} typeElement={'section'}>
        <Wrapper>
          {children}
        </Wrapper>
      </Container>
    </main>
    <Footer/>
  </ThemeProvider>
}

export default Layout;