import {BreadcrumbsProps} from '../header/breadcrumbs/breadcrumbs.types.ts';
import {ChildrenProps} from '../../../types/children.ts';

export interface LayoutProps extends ChildrenProps, BreadcrumbsProps {}