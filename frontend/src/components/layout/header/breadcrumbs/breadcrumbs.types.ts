import {Links} from '../../../app/app.helper.tsx';

type BreadcrumbsElement = [string, Links?]
export interface BreadcrumbsProps {
    elements: BreadcrumbsElement[]
}