import styled from 'styled-components';

export const BreadcrumbsStyled = styled.span`
  display: flex;
  gap: 10px;
  margin: 15px 0;
  align-items: center;
`