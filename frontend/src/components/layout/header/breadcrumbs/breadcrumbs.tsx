import React, {FC} from 'react';
import {Link} from 'react-router-dom';

import {BreadcrumbsProps} from './breadcrumbs.types.ts';
import {BreadcrumbsStyled} from './breadcrumbs.styled.tsx';
import {Links} from '../../../app/app.helper.tsx';
import Container from '../../../common/container/container';
import Text from '../../../common/text/text.tsx';
import {TextSize} from '../../../common/text/text.types.ts';

const Breadcrumbs : FC<BreadcrumbsProps> = ({elements}) => {
  return (<Container>
    <BreadcrumbsStyled>
      <Text size={TextSize.L}><Link to={Links.HOME}>Home</Link></Text>
      {elements.map(([title, link]) =>
          <React.Fragment key={title}>
            <Text size={TextSize.S}>{'>'}</Text>
            <Text size={TextSize.L}>{link ? <Link to={link}>{title}</Link> : title}</Text>
          </React.Fragment>
      )}
    </BreadcrumbsStyled>
  </Container>)
}

export default Breadcrumbs;