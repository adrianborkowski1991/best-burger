import {useLocation} from 'react-router-dom';
import {useState} from 'react';
import {useSelector} from 'react-redux';

import {DeliveryInfo, Logo, TopStyled} from './top.styled';
import CartModal from '../../cart-modal/cart-modal.tsx';
import {Links} from '../../../app/app.helper.tsx';
import Button from '../../../common/button/button';
import Container from '../../../common/container/container';
import Text from '../../../common/text/text';
import {TextSize, TextStyle} from '../../../common/text/text.types';
import deliveryIcon from '../../../../assets/delivery-icon.png';
import logo from '../../../../assets/logo.png';
import {RootState} from '../../../../store/root-reducer.ts';

const Top = () => {
  const location = useLocation();
  const [cartOpen, setCartOpen] = useState(false);
  const cartItemsList = useSelector((state: RootState) => state.cartItems.list);

  const handleButtonClick = () => {
    setCartOpen(true);
  }

  const handleCloseModal = () => {
    setCartOpen(false);
  }

  const buttonDisabled = (cartItemsList && cartItemsList.length === 0) || !cartItemsList || location.pathname === Links.ORDER;

  return (<Container>
    <TopStyled>
      <DeliveryInfo><img alt={'Delivery'} src={deliveryIcon}/><Text font={'secondary'} size={TextSize.S} textStyle={TextStyle.ITALIC}>Free delivery every day from 7:00 AM to 4:00 PM.</Text></DeliveryInfo>
      <Logo alt='Logo' src={logo}/>
      <Button disabled={buttonDisabled} handleButtonClick={handleButtonClick}>{`Cart (${cartItemsList ? cartItemsList.length : 0})`}</Button>
    </TopStyled>
    {cartOpen && <CartModal handleCloseModal={handleCloseModal}/>}
  </Container>)
}

export default Top;