import styled from 'styled-components';

export const TopStyled = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  justify-items: end;
  padding: 10px 0;

  @media (max-width: ${({theme}) => theme.breakpoints.mobileLarge}px) {
    display: flex;
    flex-direction: column;
    justify-items: center;
    gap: 15px;
  }
`
export const DeliveryInfo = styled.div`
  justify-self: start;
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 10px;
  
  & img {
    width: 20px;
  }

  ${({theme}) => `
    @media (max-width: ${theme.breakpoints.tablet}px) {
      width: calc(100% - 60px);
    }

    @media (max-width: ${theme.breakpoints.mobileLarge}px) {
      width: 100%;
      order: 1;
    }
  `}
`

export const Logo = styled.img`
  width: 160px;
  position: absolute;
  top: 5px;
  left: 50%;
  transform: translateX(-50%);

  ${({theme}) => `
    @media (max-width: ${theme.breakpoints.desktopSmall}px) {
      width: 100px;
    }
    
    @media (max-width: ${theme.breakpoints.mobileLarge}px) {
      position: relative;
    }
  `}
`