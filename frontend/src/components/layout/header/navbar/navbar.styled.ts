import styled from 'styled-components';

export const NavbarStyled = styled.nav`
  display: flex;
  height: 90px;
  text-transform: capitalize;
`

export const NavbarSelect = styled.select`
  padding: 20px;
  cursor: pointer;
  width: 100%;
  border-radius: 20px;
  margin: 10px 0;
  -moz-appearance: none;
  -webkit-appearance: none;
  appearance: none;
  background-image: url("data:image/svg+xml,<svg height='10px' width='10px' viewBox='0 0 16 16' fill='%23000000' xmlns='http://www.w3.org/2000/svg'><path d='M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z'/></svg>");
  background-repeat: no-repeat, repeat;
  background-position: right 20px top 50%, 0 0;
  text-transform: capitalize;
  ${({theme}) => `
    border: 2px solid ${theme.colors.pearl};
    background-color: ${theme.colors.antique};
  `}
  
  option {
    text-transform: capitalize;
  }
`
