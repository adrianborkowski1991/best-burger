import styled from 'styled-components';
import {NavLink} from 'react-router-dom';

export const NavLinkStyled = styled(NavLink)`
  width: 90px;
  cursor: pointer;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  padding: 20px 20px 15px;
  display: flex;

  ${({theme}) => `
    border-bottom: 5px solid ${theme.colors.antique};
    &.active{
      border-bottom-color: ${theme.colors.orange};
      
      >span{
        color: ${theme.colors.orange};
      }
    }
  `};
  
  >img{
    width: 30px;
    padding-top: 10px;
  }
`