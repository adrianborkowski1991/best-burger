import {FC} from 'react';

import {NavLinkProps} from './navlink.types.ts';
import {NavLinkStyled} from './navlink.styled.ts';
import Text from '../../../../common/text/text.tsx';

const NavLink : FC<NavLinkProps> = ({title, image, link}) => {

    return <NavLinkStyled to={link}>
        <Text>{title}</Text>
        <img src={image} alt={title} />
    </NavLinkStyled>
}

export default NavLink;