export interface NavLinkProps {
    title: string;
    image: string;
    link: string;
}