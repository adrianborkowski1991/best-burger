import {ChangeEvent, useEffect} from 'react';
import {useDispatch, useSelector } from 'react-redux';
import {useNavigate} from 'react-router-dom';

import {NavbarSelect, NavbarStyled} from './navbar.styled.ts';
import NavLink from './navlink/navlink.tsx';
import Container from '../../../common/container/container';
import useBreakpoint from '../../../../hooks/use-breakpoint.ts';
import {getAllCategoryAsync} from '../../../../store/slices/category/category-slice.ts';
import {AppDispatch} from '../../../../store/configure-store.ts';
import {RootState} from '../../../../store/root-reducer.ts';

const Navbar = () => {
  const navigate = useNavigate();
  const { breakpointCheck } = useBreakpoint();
  const dispatch = useDispatch<AppDispatch>();
  const categoryList = useSelector((state: RootState) => state.category.list);

  useEffect(()=> {
    void dispatch(getAllCategoryAsync());
  }, [dispatch])

  const handleChangeSelect = (event: ChangeEvent<HTMLSelectElement>) => {
    const link = event.target.value;
    navigate(`/${link}`);
  }

  const  isMobile = breakpointCheck('mobileSmall');

  const items = <>{categoryList && categoryList.map(item=>
      isMobile? <option value={item.name} key={item.id}>{item.name}</option> : <NavLink key={item.id} title={item.name} link={'/' + item.name} image={`${import.meta.env.VITE_SERVER_URL}${item.iconUrl}`}/>)}</>;

  return (<Container variant={'antique'}>
      <NavbarStyled>
        {categoryList && <>{isMobile ?
            <NavbarSelect onChange={handleChangeSelect}>
              {items}
            </NavbarSelect> :
            <>{items}</>
        }</>}
      </NavbarStyled>
  </Container>)
}

export default Navbar;