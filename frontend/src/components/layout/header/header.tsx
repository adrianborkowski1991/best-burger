import Navbar from './navbar/navbar';
import Top from './top/top';

const Header  = () => {
  return (<header>
    <Top/>
    <Navbar/>
  </header>)
}

export default Header;