import '@testing-library/jest-dom'
import {fireEvent,  screen} from '@testing-library/react'

import {CartModalContent} from './cart-modal.tsx';
import {getAllCartItemsAsync} from '../../../store/slices/cart/cart-slice.ts';
import {mockCartResponse} from '../../../test/mocks/response/cart-item-response.ts';
import {dispatchApp} from '../../../test/mocks/response/dispatch.ts';
import {renderWithProviders} from '../../../test/render.tsx';

beforeAll(() => {
  mockCartResponse()
})

describe('Cart Modal - remove item', () => {
  test('remove one element', async () => {
    await  dispatchApp((getAllCartItemsAsync()))

    const {container} = renderWithProviders(<CartModalContent handleCloseModal={jest.fn()} />);

    expect(container.getElementsByClassName('item').length).toBe(3);

    const removeButton = screen.getAllByRole('button', { name: /remove/i })[0];
    fireEvent.click(removeButton);

    expect(container.getElementsByClassName('item').length).toBe(2);
    expect(screen.getByRole('button', { name: /place an order/i })).not.toBeDisabled();
  })

  test('remove entire list', async () => {
    await  dispatchApp((getAllCartItemsAsync()))

    const {container} = renderWithProviders(<CartModalContent handleCloseModal={jest.fn()} />);

    expect(container.getElementsByClassName('item').length).toBe(3);

    const removeButtons = screen.getAllByRole('button', { name: /remove/i });
    removeButtons.forEach(button => {
      fireEvent.click(button);
    })

    expect(container.getElementsByClassName('item').length).toBe(0);
    expect(screen.getByText(/no products in the cart/i)).toBeInTheDocument();
    expect(screen.getByRole('button', { name: /place an order/i })).toBeDisabled();
  })

})

describe('Cart Modal - change quantity of item', () => {

  const changeValue = async (value: string, expectedValue: string) => {
    await  dispatchApp((getAllCartItemsAsync()))

    const {container} = renderWithProviders(<CartModalContent handleCloseModal={jest.fn()} />);

    const inputQuantity = container.querySelector(`input[name='quantity']`);

    if(inputQuantity){
      fireEvent.change(inputQuantity, {target: {value: value}})
    }

    expect(inputQuantity?.getAttribute('value')).toBe(expectedValue);
  }

  test('change the value to \'1\'. Correct value.', async () => {
    await changeValue('1', '1');
  })

  test('change the value to \'5\'. Correct value.', async () => {
    await changeValue('5', '5');
  })

  test('change the value to \'\'. Correct value.', async () => {
    await changeValue('', '');
  })

  test('change the value to \'a\'. Incorrect value.', async () => {
    await changeValue('a', '');
  })

  test('change the value to \'$\'. Incorrect value.', async () => {
    await changeValue('$', '');
  })

  test('change the value to \'0\'. Below the lower limit.', async () => {
    await changeValue('0', '1');
  })

  test('change the value to \'-5\'. Below the lower limit.', async () => {
    await changeValue('-5', '1');
  })

  test('change the value to \'100\'. Above the upper limit.', async () => {
    await changeValue('100', '99');
  })

})
