import styled from 'styled-components';

export const CartModalContainer = styled.div`
  width: 100vw;
  height: 100vh;
  z-index: 100;
  background-color: rgba(232, 220, 201, .5);
  display: flex;
  position: fixed;
`

export const CartModalBackground = styled.div`
  width: 100vw;
  height: 100vh;
  z-index: 101;
  position: fixed;
`

export const CartModalInside = styled.div`
  width: 60%;
  border-radius: 20px;
  margin: auto;
  padding: 20px;
  position: relative;
  z-index: 102;
  ${({theme}) =>
      `
        border: 2px solid ${theme.colors.pearl};
        background-color: ${theme.colors.antique};
      `
  };

  @media (max-width: ${({theme}) => theme.breakpoints.desktopSmall}px) {
    width: 70%;
  }
  
  @media (max-width: ${({theme}) => theme.breakpoints.desktopLarge}px) {
    width: 90%;
  }
  
  @media (max-width: ${({theme}) => theme.breakpoints.tablet}px) {
    width: 100%;
    margin: auto 15px;
    overflow-y: auto;
    height: calc(100% - 85px);
    min-height: 550px;
  }
`

export const CartModalHeader = styled.header`
  margin-top: 20px;
  height: 44px;
  text-align: center;
`

export const CartModalCloseButton = styled.button`
  position: absolute;
  top: 25px;
  right: 20px;
  cursor: pointer; center;
  & img {
    width: 24px;
  }
`

export const CartModalBody = styled.div`
  margin: 20px 0;
  
  @media (max-width: ${({theme}) => theme.breakpoints.tablet}px) {
    margin: 20px 0 65px 0;

  }
`

export const NoProduct = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 50px;

  @media (max-width: ${({theme}) => theme.breakpoints.tablet}px) {
    height: 100%;
  }
`

export const CartModalFooter = styled.footer`
  margin: 0 20px 20px 20px;
  padding: 0 20%;

  @media (max-width: ${({theme}) => theme.breakpoints.tablet}px) {
    margin: 0 0 15px 0;
    padding: 15px 15px 0 15px;
    height: 75px;
    position: fixed;
    bottom: 0;
    width: calc(100% - 30px);
    left: 15px;
    border-bottom-right-radius: 20px;
    border-bottom-left-radius: 20px;
    
    ${({theme}) => `
       background: ${theme.colors.antique};
       border: 2px solid ${theme.colors.pearl};
       border-top: 0px;
    `}
  }
`