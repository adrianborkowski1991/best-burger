export interface CartModalProps{
    handleCloseModal: (event: React.MouseEvent<HTMLDivElement | HTMLButtonElement>) => void;
}