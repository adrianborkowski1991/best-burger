import {FC, useState} from 'react';
import {useDispatch} from 'react-redux';

import {quantityConvert} from './cart-item.helper';
import {
    CartItemStyled,
    OrderItemDeleteButton,
    OrderItemImage,
    OrderItemPrice,
    OrderItemQuantity,
    OrderItemTitle
} from './cart-item.styled.tsx';
import {OrderItemProps, QuantityType} from './cart-item.types.ts';
import Button from '../../../common/button/button.tsx';
import Input from '../../../common/input/input.tsx';
import Text from '../../../common/text/text.tsx';
import {TextSize, TextWeight} from '../../../common/text/text.types.ts';
import Price from '../../../common/price/price.tsx';
import {AppDispatch} from '../../../../store/configure-store.ts';
import {putAddItemToCartAsync, putRemoveItemFromCartAsync} from '../../../../store/slices/cart/cart-slice.ts';

const CartItem : FC<OrderItemProps> = ({cartItem}) => {
    const dispatch = useDispatch<AppDispatch>();
    const [quantity, setQuantity] = useState<QuantityType>(cartItem.quantity);

    const handleRemoveItemButtonClick = async () => {
        await dispatch(putRemoveItemFromCartAsync({
            cartItem,
        }));
    }

    const handleChangeItemQuantityChange = async (event : React.ChangeEvent<HTMLInputElement>) => {
      const value = event.target.value;
      const isEmptyString = value === '';
      const convertValue = quantityConvert(value);

      setQuantity(convertValue);
    
      if(!isEmptyString){
        await dispatch(putAddItemToCartAsync({
          quantity: +convertValue,
          cartItem,
        }));
      }
    }

    return (
        <CartItemStyled className={'item'}>
            <OrderItemImage src={`${import.meta.env?.VITE_SERVER_URL}${cartItem.imageUrl}`}/>
            <OrderItemTitle><Text color={'brown'} font={'secondary'} size={TextSize.XL} weight={TextWeight.BOLD}>{cartItem.title}</Text></OrderItemTitle>
            <OrderItemPrice><Price price={cartItem.price} integerPartSize={TextSize.XL} fractionalPartSize={TextSize.L}/></OrderItemPrice>
            <OrderItemQuantity><Input value={quantity ?? ''} onChange={handleChangeItemQuantityChange} name={'quantity'} type={'number'} /></OrderItemQuantity>
            <OrderItemDeleteButton>
                <Button handleButtonClick={handleRemoveItemButtonClick} color={'red'} >Remove</Button>
            </OrderItemDeleteButton>
        </CartItemStyled>
    )
}

export default CartItem;