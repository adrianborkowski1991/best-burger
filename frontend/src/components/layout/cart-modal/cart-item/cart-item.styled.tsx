import styled from 'styled-components';

export const CartItemStyled = styled.div`
  display: grid;
  grid-template-columns: 10% 30% 20% 20% 20%;
  grid-template-areas: "image title price quantity delete-button";
  align-items: center;
  gap: 10px;

  @media (max-width: ${({theme}) => theme.breakpoints.tablet}px) {
    grid-template-columns: repeat(3, 33%);
    grid-template-areas: 
        "image image image"
        "title title price"
        "quantity delete-button delete-button";
    margin: 20px 10px;
  }
  
  ${({theme}) => `
    border-top: 1px solid ${theme.colors.pearl};

    &:last-child {
      border-bottom: 1px solid ${theme.colors.pearl};
    }
      
    @media (max-width: ${theme.breakpoints.tablet}px) {
      &:last-child {
        padding-bottom: 20px;
      }
    }
  `}
`
export const OrderItemImage = styled.img`
  grid-area: image;
  width: 75px;
  position: relative;

  @media (max-width: ${({theme}) => theme.breakpoints.tablet}px) {
    width: 50%;
    margin: auto;
    position: relative;
    top: auto;
  }
`

export const OrderItemTitle = styled.div`
  grid-area: title;
`

export const OrderItemPrice = styled.div`
  @media (max-width: ${({theme}) => theme.breakpoints.tablet}px) {
    display: flex;
    justify-content: end;
  }
`

export const OrderItemQuantity = styled.div`
  grid-area: quantity;
`

export const OrderItemDeleteButton = styled.div`
  grid-area: delete-button;
  
  @media (max-width: ${({theme}) => theme.breakpoints.tablet}px) {
    display: flex;
    justify-content: end;
  }
`