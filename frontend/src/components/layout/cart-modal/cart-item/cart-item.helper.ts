import {QuantityType} from './cart-item.types';
import {InputType} from '../../../../types/form';

export const quantityConvert = (value: InputType) : QuantityType =>  {
  const number = +value;
  if (value !== '' && !(isNaN(number))) {
    if(number < 1){
      return 1;
    }
    else if(number > 99){
      return 99;
    }
    return number;
  }
  return '';
}
