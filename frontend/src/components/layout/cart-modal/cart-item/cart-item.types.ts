import {CartItem} from '../../../../store/slices/cart/cart-slice.types.ts';

export interface OrderItemProps{
    cartItem: CartItem;
}

export type QuantityType = '' | number;