import {FC} from 'react';
import {createPortal} from 'react-dom';
import {useNavigate} from 'react-router-dom';
import {useSelector} from 'react-redux';

import {
    CartModalBody, CartModalFooter,
    CartModalHeader,
    CartModalBackground,
    CartModalContainer,
    CartModalInside, CartModalCloseButton, NoProduct
} from './cart-modal.styled.tsx';
import CartItem from './cart-item/cart-item.tsx';
import {CartModalProps} from './cart-modal.types.ts';
import {Links} from '../../app/app.helper.tsx';
import Button from '../../common/button/button.tsx';
import Text from '../../common/text/text.tsx';
import {TextSize, TextWeight} from '../../common/text/text.types.ts';
import closeIcon from '../../../assets/close-icon.png'
import {RootState} from '../../../store/root-reducer.ts';

const CartModal : FC<CartModalProps> = ({handleCloseModal}) => {
    return createPortal(
        <CartModalContent handleCloseModal={handleCloseModal}/>,
        document.getElementById('cart')!
    )
}

export const CartModalContent : FC<CartModalProps> = ({handleCloseModal}) => {
  const navigate = useNavigate();
  const cartItemsList = useSelector((state: RootState) => state.cartItems.list);
  const cartItemsSortedList = cartItemsList ? [...cartItemsList].sort((a, b) => a.title.toLowerCase().localeCompare(b.title.toLowerCase())) : [];
  const handleToOrderClick = () => {
    navigate(Links.ORDER);
  }

  return (
    <CartModalContainer>
      <CartModalBackground onClick={handleCloseModal}/>
      <CartModalInside>
        <CartModalHeader>
          <Text typeElement={'h1'} size={TextSize.XL} weight={TextWeight.BOLD} color={'brown'} font={'secondary'}>Cart</Text>
            <CartModalCloseButton onClick={handleCloseModal}>
              <img src={closeIcon} alt={'close'}/>
            </CartModalCloseButton>
        </CartModalHeader>
        <CartModalBody>
            {(cartItemsList && cartItemsList.length > 0)
                ? <>{cartItemsSortedList.map(cartItem => <CartItem key={cartItem.productId} cartItem={cartItem} />)}</> :
                <NoProduct><Text>No products in the cart.</Text></NoProduct>
            }
        </CartModalBody>
        <CartModalFooter>
            <Button disabled={Boolean(cartItemsList && cartItemsList.length === 0)} handleButtonClick={handleToOrderClick} full={String(true)}>Place an order</Button>
        </CartModalFooter>
      </CartModalInside>
    </CartModalContainer>
  )
}

export default CartModal;