import styled, {css} from 'styled-components';

import {ContainerCommon} from './container.types';
import {Colors} from '../../../style/theme';

const containerCommon = css<ContainerCommon>`
  background: ${props => props.theme.colors[props.variant as keyof Colors]};
`

export const ContainerFooter = styled.footer<ContainerCommon>`
  ${containerCommon}
`;

export const ContainerSection = styled.section<ContainerCommon>`
  ${containerCommon}
`;

export const ContainerDiv = styled.div<ContainerCommon>`
  ${containerCommon}
`;

export const ContainerInside = styled.div`
  height: 100%;
  max-width: 1248px;
  margin-left: auto;
  margin-right: auto;
  padding-left: 25px;
  padding-right: 25px;
`
