import {ChildrenProps} from '../../../types/children.ts';

export interface ContainerCommon {
  variant?: 'pearl' | 'antique';
}

export interface ContainerProps extends ChildrenProps, ContainerCommon {
  typeElement?: 'div' | 'section' | 'footer';
}

