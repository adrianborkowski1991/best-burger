import {FC} from 'react';

import {ContainerDiv, ContainerFooter, ContainerInside, ContainerSection} from './container.styled';
import {ContainerProps} from './container.types';

const Container : FC<ContainerProps> = ({children, typeElement = 'div', variant = 'pearl'}) => {

  const insideElement = <ContainerInside>{children}</ContainerInside>;

  switch(typeElement){
    case 'section': return <ContainerSection variant={variant}>{insideElement}</ContainerSection>
    case 'footer': return <ContainerFooter variant={variant}>{insideElement}</ContainerFooter>
    default: return <ContainerDiv variant={variant}>{insideElement}</ContainerDiv>
  }
}

export default Container;