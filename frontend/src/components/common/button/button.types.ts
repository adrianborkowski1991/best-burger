import {ChildrenProps} from '../../../types/children.ts';

import {Colors} from '../../../style/theme.tsx';

export interface ButtonCommon {
  full?: string;
  color?: keyof Colors;
}

export interface ButtonProps extends ChildrenProps, ButtonCommon{
  handleButtonClick: () => void;
  disabled?: boolean;
}