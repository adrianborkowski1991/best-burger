import styled from 'styled-components';

import {ButtonCommon} from './button.types.ts';

export const ButtonStyled = styled.button<ButtonCommon>`
  height: 44px;
  background-color: ${({theme, color})=> color ? theme.colors[color] : theme.colors.brown};
  text-align: center;
  border-radius: 10px;
  padding: 8px 30px;
  cursor: pointer;
  ${({full}) => full ? 'max-width: 100%; width: 100%;' : 'max-width: 150px'};

  &:disabled{
    opacity: .6;
    cursor: auto;
  }

  @media (max-width: ${({theme}) => theme.breakpoints.mobileLarge}px) {
    max-width: 100%;
    width: 100%;
  }
`