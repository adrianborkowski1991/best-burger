import {FC} from 'react';

import {ButtonStyled} from './button.styled';
import {ButtonProps} from './button.types';
import Text from '../text/text';

const Button : FC<ButtonProps> = ({children, handleButtonClick, disabled, full, color }) => {
  return (<ButtonStyled onClick={handleButtonClick} className={'button'} disabled={disabled} full={full} color={color}><Text color={'white'} font={'secondary'} >{children}</Text></ButtonStyled>)
}

export default Button;