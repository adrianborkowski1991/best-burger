import styled from 'styled-components';

export const LoaderStyled = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 106;
  opacity: 0.3;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${({theme}) => theme.colors.pearl};
`

export const LoadedBackground = styled.div`
  opacity: 0.6;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 105;
`