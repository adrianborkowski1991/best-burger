import {BounceLoader} from 'react-spinners';

import {LoadedBackground, LoaderStyled} from './loader.styled.tsx';
import {theme} from '../../../style/theme.tsx';

const Loader = () => {
  return (
      <LoaderStyled>
        <LoadedBackground />
        <BounceLoader color={theme.colors.brown} />
      </LoaderStyled>)
}

export default Loader;