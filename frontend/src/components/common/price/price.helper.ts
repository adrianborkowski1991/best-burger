export const splitNumberIntoPriceParts = (number: number) => {
    const integerPart = Math.floor(number);
    const fractionalPart = (number - integerPart).toFixed(2);

    return [integerPart.toString(), fractionalPart.slice(2)];
}
