import {TextSize} from '../text/text.types.ts';

export interface PriceProps {
    price: number;
    integerPartSize: TextSize;
    fractionalPartSize: TextSize;
}