import {FC} from 'react';

import {PriceProps} from './price.types.ts';
import {PriceStyled} from './price.styled.tsx';
import {splitNumberIntoPriceParts} from './price.helper.ts';
import Text from '../text/text.tsx';

const Price : FC<PriceProps>= ({price, integerPartSize, fractionalPartSize}) => {

    const [integerPart, fractionalPart] = splitNumberIntoPriceParts(price);

    return (
        <PriceStyled>
            <Text size={integerPartSize} color={'brown'} font={'secondary'}>{integerPart}</Text>
            <Text typeElement={'sup'} size={fractionalPartSize} color={'brown'} font={'secondary'}>{fractionalPart}</Text>
        </PriceStyled>
    )
}

export default Price;