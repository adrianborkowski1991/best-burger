import styled from 'styled-components';

import {priceCommon} from '../../../utils/common-styled-helper.tsx';

export const PriceStyled = styled.div`
  ${priceCommon}
`