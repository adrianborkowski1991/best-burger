import styled, {css} from 'styled-components';

import {TextStyledCommon} from './text.types';
import {Colors, Fonts} from '../../../style/theme';

export const textCommon = css<TextStyledCommon>`
  ${props => `
    color: ${props.theme.colors[props.color as keyof Colors]};
    font-family: ${props.theme.fonts[props.font as keyof Fonts]};
    font-weight: ${props.weight};
    font-style: ${props.$textStyle};
    font-size: ${props.size}px;
    line-height: 1.2;
    text-decoration: ${props.decoration};
  `};
`

export const TextParagraph = styled.p<TextStyledCommon>`
  ${textCommon}
`;

export const TextSpan = styled.span<TextStyledCommon>`
  ${textCommon}
`;

export const TextSup = styled.sup<TextStyledCommon>`
  ${textCommon}
`;

export const TextH1 = styled.h1<TextStyledCommon>`
  ${textCommon}
`;

export const TextH2 = styled.h2<TextStyledCommon>`
  ${textCommon}
`;

export const TextH3 = styled.h3<TextStyledCommon>`
  ${textCommon}
`;