import {FC} from 'react';

import {TextH1, TextH2, TextH3, TextParagraph, TextSpan, TextSup} from './text.styled';
import {TextProps, TextSize, TextStyle, TextWeight} from './text.types';

const Text: FC<TextProps> = ({children, typeElement= 'span', color = 'black', font = 'primary', weight = TextWeight.REGULAR, size = TextSize.M, textStyle = TextStyle.NORMAL, decoration = 'none'}) => {

  const restProps = {color, font, weight, size, decoration};

  switch(typeElement){
    case 'p': return <TextParagraph {...restProps} $textStyle={textStyle}>{children}</TextParagraph>
    case 'sup': return <TextSup {...restProps} $textStyle={textStyle}>{children}</TextSup>
    case 'h1': return <TextH1 {...restProps} $textStyle={textStyle}>{children}</TextH1>
    case 'h2': return <TextH2 {...restProps} $textStyle={textStyle}>{children}</TextH2>
    case 'h3': return <TextH3 {...restProps} $textStyle={textStyle}>{children}</TextH3>
    default: return <TextSpan {...restProps} $textStyle={textStyle}>{children}</TextSpan>
  }
}

export default Text;