import {Colors, Fonts} from '../../../style/theme';
import {ChildrenProps} from '../../../types/children.ts';

interface TextCommon {
  color?: keyof Colors;
  font?: keyof Fonts;
  size?: TextSize;
  weight?: TextWeight;
  decoration?: 'none' | 'underline';
}

export interface TextPropsCommon extends TextCommon{
  typeElement?: 'p' | 'span' | 'sup' | 'h1' | 'h2' | 'h3';
  textStyle?: TextStyle;
}

export interface TextStyledCommon extends TextCommon{
  $textStyle?: TextStyle;
}

export enum TextStyle{
  NORMAL = 'normal',
  ITALIC = 'italic'
}

export enum TextSize{
  XS = 10,
  S = 14,
  M = 16,
  L = 18,
  XL = 24,
  XXL = 50
}

export enum TextWeight{
  REGULAR = 400,
  MEDIUM = 500,
  BOLD = 700
}

export interface TextProps extends TextPropsCommon, ChildrenProps {}