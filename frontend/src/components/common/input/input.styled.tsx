import styled from 'styled-components';

import {InputCommon} from './input.types.ts';

export const InputLabel = styled.label`
  height: 65px;
  position: relative;
  
  & span {
    position: absolute;
    bottom: 8px;
    left: 15px;
  }
`
export const InputStyled = styled.input<InputCommon>`
  width: 100%;
  height: 44px;
  border-radius: 20px;
  margin-bottom: 0;
  padding: 12px 15px;
  font-size: 16px;

  ${({theme, error}) => `
    border: 1px solid ${error ? theme.colors.red : theme.colors.brown};
    background-color: ${theme.colors.pearl};
    color: ${theme.colors.brown};
  `}`;