
import {FC} from 'react';

import {InputProps} from './input.types.ts';
import {InputLabel, InputStyled} from './input.styled.tsx';
import Text from '../text/text';
import {TextSize} from '../text/text.types';

const Input: FC<InputProps> = (props) => {
    const { errorMessage, error, ...restProps } = props;

    return (<InputLabel><InputStyled error={error} {...restProps} />
        {error && errorMessage && <Text color={'red'} size={TextSize.XS}>{errorMessage}</Text>}
    </InputLabel>)

}

export default Input;