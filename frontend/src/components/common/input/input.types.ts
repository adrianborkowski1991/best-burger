import {InputType} from '../../../types/form';

export interface InputCommon{
    error?: boolean;
}

export interface InputConfigCommon {
    placeholder?: string;
    type: 'text' | 'number';
    errorMessage?: string;
}

export interface InputProps extends InputCommon, InputConfigCommon{
    value: InputType;
    onChange: (event : React.ChangeEvent<HTMLInputElement>) => void;
    name: string;
}