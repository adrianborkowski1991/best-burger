import {
  OperationsRequestState,
  ReduxState,
  ReduxStateObject,
  RequestState,
} from './slices-utils.types';
import {ResponseRedux} from '../fetch-utils/fetch-utils.types.ts';
import {BasicObject} from '../../types/basic.ts';

export function createRequestStateReducersObject () {
  return {
    pending: (key: keyof OperationsRequestState, prevState: OperationsRequestState) => {
      return {
        state: {
          ...prevState,
          [key]: RequestState.LOADING
        }
      }
    },
    fulfilled: <T extends BasicObject | string> (response: ResponseRedux<T>, key: keyof OperationsRequestState, prevState: ReduxStateObject<T>) : ReduxStateObject<T> => {
      const newState : ReduxStateObject<T> = {
        ...prevState,
        state: {
          ...prevState.state,
          [key]: RequestState.SUCCESS
        }
      };

      if(Array.isArray(response.data)){
        newState.list = response.data as T[];
      }
      else{
        newState.view = response.data as T;
      }

      return newState;
    },
     rejected: (key: keyof OperationsRequestState, prevState: OperationsRequestState) => {
      return {
        state: {
          ...prevState,
          [key]: RequestState.ERROR
        }
      }
    },
  }
}

export const initializeState = <T>() : ReduxState<T> =>{
  return {
    response: null,
    state: RequestState.INITIAL
  }
}
