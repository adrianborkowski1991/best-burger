
import { ResponseRedux } from '../fetch-utils/fetch-utils.types';
import { BasicObject } from '../../types/basic.ts';
export enum OperationData {
  GET_ALL = 'getAll', GET = 'get', POST = 'post', PUT = 'put', DELETE = 'delete'
}

export enum RequestState {
  INITIAL = 'INITIAL', LOADING='LOADING', SUCCESS='SUCCESS', ERROR = 'ERROR'
}

export interface ReduxState<T> {
  response: ResponseRedux<T> | null;
  state: RequestState;
}

export interface ReduxStateObject<T extends BasicObject | string> extends BasicObject{
  response: Response | null;
  list: T[] | null;
  view: T | null;
  state: OperationsRequestState;
}

export interface OperationsRequestState {
  getAll: RequestState;
  get: RequestState;
  post: RequestState;
  put: RequestState;
  delete: RequestState;
}
