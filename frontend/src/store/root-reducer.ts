import { combineReducers } from 'redux';
import { Reducer } from '@reduxjs/toolkit';

import { CartItemsRedux } from './slices/cart/cart-slice.types.ts';
import { cartItemsSlice } from './slices/cart/cart-slice.ts';
import { categorySlice } from './slices/category/category-slice.ts';
import { CategoryRedux } from './slices/category/category-slice.types.ts';
import { orderSlice } from './slices/order/order-slice.ts';
import { OrderRedux } from './slices/order/order-slice.types.ts';
import { productSlice } from './slices/product/product-slice.ts';
import { ProductRedux } from './slices/product/product-slice.types.ts';

export interface RootState {
  category: CategoryRedux;
  product: ProductRedux;
  cartItems: CartItemsRedux;
  orderSlice: OrderRedux;
}

const rootReducer : Reducer<RootState> = combineReducers({
  category: categorySlice.reducer,
  product: productSlice.reducer,
  cartItems: cartItemsSlice.reducer,
  orderSlice: orderSlice.reducer
});

export default rootReducer;

