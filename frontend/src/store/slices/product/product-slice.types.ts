import {BasicObject} from '../../../types/basic.ts';
import {ReduxStateObject} from '../../slice-utils/slices-utils.types.ts';

export type ProductRedux = ReduxStateObject<Product>

export interface Product extends BasicObject{
    title : string,
    price: number,
    imageUrl: string,
    new: boolean,
    categoryId: number
}
