import {createAsyncThunk, createSlice, Slice} from '@reduxjs/toolkit';

import {APIPath, MethodHTTP} from '../../fetch-utils/fetch-utils.types.ts';
import {createRequestStateReducersObject, initializeState} from '../../slice-utils/slices-utils.ts';
import {fetchUtils} from '../../fetch-utils/fetch-utils.ts';
import {generateLink} from '../../../utils/link-helper.ts';
import {OperationData} from '../../slice-utils/slices-utils.types.ts';
import {Product, ProductRedux} from './product-slice.types.ts';

export const getAllProductAsync = createAsyncThunk(
    'product/get-all',
    async (filter: string) => {

        return await fetchUtils<Product>({
            method: MethodHTTP.GET,
            path: generateLink(APIPath.PRODUCT, [[':filter', filter]])
        });
    }
);

export const productSlice : Slice<ProductRedux> = createSlice({
    name: 'product',
    initialState: {
        ...initializeState<Product>
    }  as ProductRedux,
    reducers: {},
    extraReducers: builder => {
        builder
            .addCase(getAllProductAsync.pending, (state) => {
                Object.assign(state, createRequestStateReducersObject().pending(OperationData.GET_ALL, state.state));
                state.list = null;
            })
            .addCase(getAllProductAsync.fulfilled, (state, action) => {
                Object.assign(state, createRequestStateReducersObject().fulfilled<Product>(action.payload, OperationData.GET_ALL, state));
            })
            .addCase(getAllProductAsync.rejected, (state) => {
                Object.assign(state, createRequestStateReducersObject().rejected(OperationData.GET_ALL, state.state));
            })
    }
});