import { createAsyncThunk, createSlice, Slice } from '@reduxjs/toolkit';

import { APIPath, MethodHTTP } from '../../fetch-utils/fetch-utils.types';
import { CartItem, CartItemsRedux, ItemBody} from './cart-slice.types.ts';
import { createRequestStateReducersObject, initializeState } from '../../slice-utils/slices-utils';
import { fetchUtils } from '../../fetch-utils/fetch-utils';
import {OperationData} from '../../slice-utils/slices-utils.types.ts';

export const getAllCartItemsAsync = createAsyncThunk(
  'cartItems/get-all',
  async () => {

    return await fetchUtils<CartItem>({
      method: MethodHTTP.GET,
      path: APIPath.CART_ITEMS,
    });
  }
);

export const putAddItemToCartAsync = createAsyncThunk(
    'cartItems/put-add-item',
    async (itemBody: ItemBody) => {

      return await fetchUtils<CartItem>({
        method: MethodHTTP.PUT,
        path: APIPath.CART_ITEMS_ADD,
        data: {
          body: {
            quantity: itemBody.quantity,
            productId: itemBody.cartItem.productId,
          }
        }
      });
    }
);

export const putRemoveItemFromCartAsync = createAsyncThunk(
    'cartItems/put-remove-item',
    async (itemBody: ItemBody) => {

      return await fetchUtils<CartItem>({
        method: MethodHTTP.PUT,
        path: APIPath.CART_ITEMS_REMOVE,
        data: {
          body: {
            quantity: itemBody.quantity,
            productId: itemBody.cartItem.productId,
          }
        }
      });
    }
);

export const cartItemsSlice : Slice<CartItemsRedux> = createSlice({
  name: 'cartItems',
  initialState: {
    ...initializeState<CartItem>
  }  as CartItemsRedux,
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(getAllCartItemsAsync.pending, (state) => {
        Object.assign(state, createRequestStateReducersObject().pending(OperationData.GET_ALL, state.state));
      })
      .addCase(getAllCartItemsAsync.fulfilled, (state, action) => {
        Object.assign(state, createRequestStateReducersObject().fulfilled<CartItem>(action.payload, OperationData.GET_ALL, state));
      })
      .addCase(getAllCartItemsAsync.rejected, (state) => {
        Object.assign(state, createRequestStateReducersObject().rejected(OperationData.GET_ALL, state.state));
      })

      .addCase(putAddItemToCartAsync.pending, (state, payload) => {
        Object.assign(state, createRequestStateReducersObject().pending(OperationData.PUT, state.state));
        const isItem = state.list?.some(item => item.productId === payload.meta.arg.cartItem.productId);

        if(isItem && state.list){
          state.list = state.list.map(element => {
            const elementCopy = {...element};
            if(elementCopy.productId === payload.meta.arg.cartItem.productId){
              elementCopy.quantity = payload.meta.arg.quantity ?? 1;
              return elementCopy;
            }
            return element;
          });
        } else if (state.list) {
          state.list = [...state.list, {...payload.meta.arg.cartItem}]
        }
        else{
          state.list = [{...payload.meta.arg.cartItem}]
        }

      })
      .addCase(putAddItemToCartAsync.rejected, (state) => {
        Object.assign(state, createRequestStateReducersObject().rejected(OperationData.PUT, state.state));
      })

      .addCase(putRemoveItemFromCartAsync.pending, (state, action) => {
        Object.assign(state, createRequestStateReducersObject().pending(OperationData.PUT, state.state));
        state.list = state.list ? state.list.filter(element => element.productId !== action.meta.arg.cartItem.productId) : null;
      })
      .addCase(putRemoveItemFromCartAsync.rejected, (state) => {
        Object.assign(state, createRequestStateReducersObject().rejected(OperationData.PUT, state.state));
      })
  }
});
