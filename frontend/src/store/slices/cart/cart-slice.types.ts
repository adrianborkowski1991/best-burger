import { BasicObject } from '../../../types/basic.ts';
import { ReduxStateObject} from '../../slice-utils/slices-utils.types';

export type CartItemsRedux = ReduxStateObject<CartItem>

export interface CartItem extends BasicObject{
  quantity: number,
  productId: number,
  title: string,
  price: number,
  imageUrl: string,
}

export interface ItemBody {
  quantity?: number,
  cartItem: CartItem,
}

