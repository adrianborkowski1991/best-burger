
import {ReduxStateObject} from '../../slice-utils/slices-utils.types';
import { BasicObject } from '../../../types/basic.ts';

export type CategoryRedux = ReduxStateObject<Category>

export interface Category extends BasicObject{
  name : string,
  iconUrl: string
}
