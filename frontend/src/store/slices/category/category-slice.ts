import { createAsyncThunk, createSlice, Slice } from '@reduxjs/toolkit';

import { APIPath, MethodHTTP } from '../../fetch-utils/fetch-utils.types';
import {createRequestStateReducersObject, initializeState} from '../../slice-utils/slices-utils';
import { Category, CategoryRedux} from './category-slice.types.ts';
import { fetchUtils } from '../../fetch-utils/fetch-utils';
import { OperationData } from '../../slice-utils/slices-utils.types';

export const getAllCategoryAsync = createAsyncThunk(
  'category/get-all',
  async () => {

    return await fetchUtils<Category>({
      method: MethodHTTP.GET,
      path: APIPath.CATEGORY
    });
  }
);

export const categorySlice : Slice<CategoryRedux> = createSlice({
  name: 'category',
  initialState: {
    ...initializeState<Category>
  }  as CategoryRedux,
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(getAllCategoryAsync.pending, (state) => {
        Object.assign(state, createRequestStateReducersObject().pending(OperationData.GET_ALL, state.state));
      })
      .addCase(getAllCategoryAsync.fulfilled, (state, action) => {
        Object.assign(state, createRequestStateReducersObject().fulfilled<Category>(action.payload, OperationData.GET_ALL, state));
      })
      .addCase(getAllCategoryAsync.rejected, (state) => {
        Object.assign(state, createRequestStateReducersObject().rejected(OperationData.GET_ALL, state.state));
      })

  }
});
