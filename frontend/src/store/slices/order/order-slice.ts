import { createAsyncThunk, createSlice, Slice } from '@reduxjs/toolkit';

import { APIPath, MethodHTTP } from '../../fetch-utils/fetch-utils.types';
import { createRequestStateReducersObject, initializeState} from '../../slice-utils/slices-utils';
import { fetchUtils } from '../../fetch-utils/fetch-utils';
import { generateLink} from '../../../utils/link-helper.ts';
import { Order} from '../../../components/pages/order/order.types.ts';
import { OperationData } from '../../slice-utils/slices-utils.types';
import { OrderRedux}  from './order-slice.types.ts';

export const postOrderAsync = createAsyncThunk(
  'order/post',
  async (order: Order) => {

    return await fetchUtils<string>({
      method: MethodHTTP.POST,
      path: APIPath.ORDER,
      data: {
        body: order
      }
    });
  }
);

export const putOrderPaidAsync = createAsyncThunk(
    'order/put-paid',
    async (idOrder: string) => {

      return await fetchUtils<string>({
        method: MethodHTTP.PUT,
        path: generateLink(APIPath.ORDER_PAID, [[':id', idOrder]])
      });
    }
);


export const orderSlice : Slice<OrderRedux> = createSlice({
  name: 'order',
  initialState: {
    ...initializeState<string>
  }  as OrderRedux,
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(postOrderAsync.pending, (state) => {
        Object.assign(state, createRequestStateReducersObject().pending(OperationData.POST, state.state));
      })
      .addCase(postOrderAsync.fulfilled, (state, action) => {
        Object.assign(state, createRequestStateReducersObject().fulfilled<string>(action.payload, OperationData.POST, state));
      })
      .addCase(postOrderAsync.rejected, (state) => {
        Object.assign(state, createRequestStateReducersObject().rejected(OperationData.POST, state.state));
      })

      .addCase(putOrderPaidAsync.pending, (state) => {
        Object.assign(state, createRequestStateReducersObject().pending(OperationData.PUT, state.state));
      })
      .addCase(putOrderPaidAsync.fulfilled, (state, action) => {
        Object.assign(state, createRequestStateReducersObject().fulfilled<string>(action.payload, OperationData.PUT, state));
      })
      .addCase(putOrderPaidAsync.rejected, (state) => {
        Object.assign(state, createRequestStateReducersObject().rejected(OperationData.PUT, state.state));
      })
  }
});
