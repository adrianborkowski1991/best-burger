
import {ReduxStateObject} from '../../slice-utils/slices-utils.types';

export type OrderRedux = ReduxStateObject<string>
