export enum APIPath {
  CATEGORY = 'category',
  PRODUCT = 'product/:filter',
  CART_ITEMS = 'cart',
  CART_ITEMS_ADD = 'cart/add-product',
  CART_ITEMS_REMOVE = 'cart/remove-product',
  ORDER = 'order',
  ORDER_PAID = 'order/paid/:id'
}

export enum MethodHTTP {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE'
}
export interface FetchConfig {
  path: APIPath | string;
  method: MethodHTTP;
  data?: {
    body?: object,
    query?: string;
  };
  headers?: HeadersInit;
}

export enum OperationStatus {
  ERROR='ERROR',
  SUCCESS='SUCCESS'
}
export interface ResponseRedux<T>{
  message?: string;
  status?: number;
  operationStatus: OperationStatus;
  data: T | T[];
}
