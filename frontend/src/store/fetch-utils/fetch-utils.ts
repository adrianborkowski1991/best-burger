import axios from 'axios';

import {FetchConfig, MethodHTTP, ResponseRedux} from './fetch-utils.types';

export const axiosConfig = axios.create(
    {
        withCredentials: true,
        baseURL: `${import.meta.env?.VITE_SERVER_URL}/api/`,
        headers: {
            'Content-Type': 'application/json'
        },
    },
);

export const fetchUtils = async <T>(config: FetchConfig) => {
  const { path, data, headers,  method } = config;

  const body = JSON.stringify(data?.body);
  const URL = `${path}${data?.query ? `?${data.query}` : '' }`;
  const axiosHeaders = {
      headers: {...headers}
  }

    try {
        let response;
        switch (method) {
            case MethodHTTP.POST:
                response = await axiosConfig.post(URL, body, axiosHeaders);
                break;
            case MethodHTTP.PUT:
                response = await axiosConfig.put(URL, body, axiosHeaders);
                break;
            default:
                response = await axiosConfig.get(URL, axiosHeaders);
        }

        return response.data as ResponseRedux<T>;
  } catch (error) {
    throw new Error('Request failed');
  }
};
