import {InputConfigCommon} from '../components/common/input/input.types.ts';

export type InputType = string | number;

export interface InputConfig<T extends Record<keyof T, InputType>> extends InputConfigCommon {
    name: keyof T;
    isValid?: (value: InputType) => boolean;
    isRequired: boolean;
}

export interface InputState<T extends Record<keyof T, InputType>> extends InputConfig<T> {
    isError: boolean;
    isTouch: boolean;
    value: InputType;
}

export type ValidatorFunction = (value: InputType) => boolean;